import {html, LitElement} from 'lit';
import {customElement, property, state} from 'lit/decorators';
import '@vaadin/menu-bar';
import {createTemplate} from "Frontend/icons/get-icons";
import {Icon} from "@vaadin/icon";
import '@vaadin/vertical-layout';

import {applyTheme} from "Frontend/generated/theme";

const template = createTemplate();
document.head.appendChild(template.content);


@customElement('left-side-menu-bar')
export class LeftSideMenuBar extends LitElement {
    protected createRenderRoot() {
        const root = super.createRenderRoot();
        applyTheme(root);
        return root;
    }

    private anchorAddEvent = new CustomEvent<string>("add-node-event",{detail: 'anchor-add'});
    private attributeAddEvent = new CustomEvent<string>("add-node-event",{detail: 'attribute-add'});
    private composedAttAddEvent = new CustomEvent<string>("add-node-event",{detail: 'attribute-composed-add'});
    private historicalAttAddEvent = new CustomEvent<string>("add-node-event",{detail: 'attribute-his-add'});
    private tieAddEvent = new CustomEvent<string>("add-node-event",{detail: 'tie-add'});
    private historicalTieAddEvent = new CustomEvent<string>("add-node-event",{detail: 'tie-his-add'});
    private anchoredTieAddEvent = new CustomEvent<string>("add-node-event",{detail: 'tie-a-add'});
    private anchoredHistTieAddEvent = new CustomEvent<string>("add-node-event",{detail: 'tie-his-a-add'});
    private tieSelfAddEvent = new CustomEvent<string>("add-node-event",{detail: 'tie-self-add'});
    private txAnchorAddEvent = new CustomEvent<string>("add-node-event",{detail: 'anchor-tx-add'});
    private knotAddEvent = new CustomEvent<string>("add-node-event",{detail: 'knot-add'});
    private connectEvent = new CustomEvent<string>("add-node-event",{detail: 'connect'});
    private connectAnchorEvent = new CustomEvent<string>("add-node-event",{detail: 'connect-anchor'});


    private anchorMenuBarItem = {component: this.createItem('anchor-add', 'lean-di-icons', this.anchorAddEvent)};
    private attributeMenuBarItem = {component: this.createItem('attribute-add', 'lean-di-icons', this.attributeAddEvent)};
    private composedAttributeMenuBarItem = {component: this.createItem('attribute-composed-add', 'lean-di-icons', this.composedAttAddEvent)};
    private historicalAttributeMenuBarItem = {component: this.createItem('attribute-his-add', 'lean-di-icons', this.historicalAttAddEvent)};
    private tieMenuBarItem = {component: this.createItem('tie-add', 'lean-di-icons', this.tieAddEvent)};
    private historicalTieMenuBarItem = {component: this.createItem('tie-his-add', 'lean-di-icons', this.historicalTieAddEvent)};
    private anchoredTieMenuBarItem = {component: this.createItem('tie-a-add', 'lean-di-icons', this.anchoredTieAddEvent)};
    private anchoredHistTieMenuBarItem = { component: this.createItem('tie-his-a-add', 'lean-di-icons', this.anchoredHistTieAddEvent)};
    private tieSelfMenuBarItem = { component: this.createItem('tie-self-add', 'lean-di-icons', this.tieSelfAddEvent)};
    private txAnchorMenuBarItem = { component: this.createItem('anchor-tx', 'lean-di-icons', this.txAnchorAddEvent)};
    private knotMenuBarItem = { component: this.createItem('knot', 'lean-di-icons', this.knotAddEvent)};
    private connectMenuBarItem = { component: this.createItem('connect', 'lean-di-icons', this.connectEvent)};
    private connectAnchorMenuBarItem = { component: this.createItem('connect', 'lean-di-icons', this.connectAnchorEvent)};


    private undoEvent = new CustomEvent<string>("undo-event",{detail: 'undo'});
    private redoEvent = new CustomEvent<string>("redo-event",{detail: 'redo'});
    private downloadEvent = new CustomEvent<string>("download-event",{detail: 'download'});

    createItem(iconName: string, iconRepo: string, event: CustomEvent<string>) {
        const item = document.createElement('vaadin-context-menu-item');
        const icon = new Icon();
        icon.setAttribute('icon', `${iconRepo}:${iconName}`)
        item.setAttribute('theme', "icon")
        item.onclick = (): void => {this.dispatchEvent(event)};
        item.appendChild(icon);
        return item;
    }


    @state()
    public items = [this.anchorMenuBarItem];

    @property()
    private chosenNodeType = 0;


    @property()
    private itemSetView: {[element: number] : any[]} = {
        0: [this.anchorMenuBarItem],
        1: [
            this.attributeMenuBarItem,
            this.composedAttributeMenuBarItem,
            this.historicalAttributeMenuBarItem,
            this.anchoredTieMenuBarItem,
            this.anchoredHistTieMenuBarItem,
            // this.tieSelfMenuBarItem,
        ],
        2: [this.anchorMenuBarItem],
        3: [],
        4: [],
        5: [
            this.attributeMenuBarItem,
            this.composedAttributeMenuBarItem,
            this.historicalAttributeMenuBarItem,
            this.anchorMenuBarItem
        ],
        6: [this.connectMenuBarItem],
        7: [
            this.tieMenuBarItem,
            this.anchoredTieMenuBarItem,
            this.historicalTieMenuBarItem,
            this.anchoredHistTieMenuBarItem,
            this.txAnchorMenuBarItem
        ],
        8: [this.connectAnchorMenuBarItem]
    }

    render() {
        return html`
            <vaadin-horizontal-layout>
                <vaadin-menu-bar
                        .items="${this.itemSetView[this.chosenNodeType]}"
                        theme="icon"
                        style=" background-color: hsla(0, 0%, 100%, 0.3);"
                >
                </vaadin-menu-bar>  
            </vaadin-horizontal-layout>

        `;
    }
}