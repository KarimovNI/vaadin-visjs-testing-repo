import {html, LitElement} from 'lit';
import {customElement} from 'lit/decorators';
import '@vaadin/menu-bar';
import {createTemplate} from "Frontend/icons/get-icons";
import {Icon} from "@vaadin/icon";
import '@vaadin/vertical-layout';
import {TextField} from "@vaadin/text-field";
import {applyTheme} from "Frontend/generated/theme";
import {FormLayout} from "@vaadin/form-layout";

const template = createTemplate();
document.head.appendChild(template.content);


@customElement('right-side-menu-bar')
export class RightSideMenuBar extends LitElement {
    protected createRenderRoot() {
        const root = super.createRenderRoot();
        applyTheme(root);
        return root;
    }

    private zoomPlusEvent = new CustomEvent<string>("zoom-event",{detail: 'zoom-plus'});
    private zoomMinusEvent = new CustomEvent<string>("zoom-event",{detail: 'zoom-minus'});
    private pinEvent = new CustomEvent<string>("pin-event",{detail: 'pin'});
    private undoEvent = new CustomEvent<string>("undo-event",{detail: 'undo'});
    private redoEvent = new CustomEvent<string>("redo-event",{detail: 'redo'});
    private downloadEvent = new CustomEvent<string>("download-event",{detail: 'download'});
    private settingsEvent = new CustomEvent<string>("44",{detail: 'download'});

    createItem(iconName: string, iconRepo: string, event?: CustomEvent<string> | null) {
        const item = document.createElement('vaadin-context-menu-item');
        const icon = new Icon();
        icon.setAttribute('icon', `${iconRepo}:${iconName}`)
        item.setAttribute('theme', "icon")
        item.id = iconName;
        if(event != null){
            item.onclick = (): void => {this.dispatchEvent(event)};
        }
        item.appendChild(icon);
        return item;
    }

    private createSearchTextBox(){
        let item = new TextField();
        item.placeholder = "Имя анкера или мнемоник";
        item.style.width='300px';
        item.oninput = () => this.dispatchEvent(new CustomEvent<string>("search-event",{detail: item.value}));
        return item;
    }
    private createSettingsLayout(){
        let layout = new FormLayout();
        layout.style.width = '300px';
        let item = new TextField();
        item.placeholder = "Имя анкера или мнемоник";
        item.style.width='300px';
        item.oninput = () => this.dispatchEvent(new CustomEvent<string>("search-event",{detail: item.value}));
        layout.appendChild(item);
        return layout;
    }

    private itemsForZoomAndSearchMenuBar = [
        {component: this.createItem('pin', 'vaadin', this.pinEvent)},
        {component: this.createItem('search-plus', 'vaadin', this.zoomPlusEvent)},
        {component: this.createItem('search-minus', 'vaadin', this.zoomMinusEvent)},
        {component: this.createItem('search', 'vaadin'), children: [{ component: this.createSearchTextBox()}]},
        {component: this.createItem('undo', 'lumo', this.undoEvent)},
        {component: this.createItem('redo', 'lumo', this.redoEvent)},
        {component: this.createItem('download', 'lumo', this.downloadEvent)},
        {component: this.createItem('cog', 'lumo'), children: [{ component: this.createSettingsLayout()}]},

    ];

    render() {
        return html`
            <vaadin-menu-bar
                    .items="${this.itemsForZoomAndSearchMenuBar}"
                    theme="icon"
                    style=" background-color: hsla(0, 0%, 100%, 0.3);"
            >
            </vaadin-menu-bar>
        `;
    }
}