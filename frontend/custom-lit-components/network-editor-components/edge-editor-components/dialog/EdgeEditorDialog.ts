
// @ts-nocheck

import {html, LitElement} from 'lit';
import {customElement, state} from 'lit/decorators.js';

import '@vaadin/button';
import '@vaadin/dialog';
import '@vaadin/text-field';
import '@vaadin/vertical-layout';
import {dialogFooterRenderer, dialogRenderer} from '@vaadin/dialog/lit.js';
import type {DialogOpenedChangedEvent} from '@vaadin/dialog';
import {applyTheme} from 'Frontend/generated/theme';
import {property} from "lit/decorators";

@customElement('dialog-edge')
export class EdgeEditorDialog extends LitElement {
    protected createRenderRoot() {
        const root = super.createRenderRoot();
        // Apply custom theme (only supported if your app uses one)
        applyTheme(root);
        return root;
    }

    @state()
    public dialogOpened = false;

    @property()
    public role: string | null = null;
    @property()
    public identifier: boolean = false;
    @property()
    public description: string | null = null;

    private buffered: boolean = false;

    render() {
        return html`
            <vaadin-dialog
                    .noCloseOnOutsideClick=${true}
                    header-title="Edit edge"
                    .opened="${this.dialogOpened}"
                    @opened-changed="${(e: DialogOpenedChangedEvent) => {e.stopImmediatePropagation();this.dialogOpened = e.detail.value}}"
                    ${dialogRenderer(this.renderDialog, [])}
                    ${dialogFooterRenderer(this.renderFooter, [])}
            ></vaadin-dialog>
        `;
    }

    // @ts-ignore
    private renderDialog = () => html`
        <vaadin-vertical-layout style="align-items: stretch; width: 18rem; max-width: 100%;">
            <vaadin-text-field label="Role" @input="${(e: Event) => {this.role = e.target!.value; this.buffered = true;}}"
                               value="${this.role}"></vaadin-text-field>
            <br>
            <vaadin-checkbox label="Identifier" .checked="${this.identifier}" @checked-changed="${(e: Event) => {this.identifier = e.detail!.value; this.buffered = true;}}"            
            ></vaadin-checkbox>
            <br>
            <vaadin-text-area
                    label="Comment"
                    style="min-height: 200px"
                    .value="${this.description}"
                    @input="${(e: Event) => {this.description = e.target!.value; this.buffered = true;}}"
            ></vaadin-text-area>
        </vaadin-vertical-layout>
    `;

    private renderFooter = () => html`
        <vaadin-button @click="${() => {
        this.close();
        this.buffered = false;
        this.dispatchEvent(new Event('deselect-edge'))
    }}">Cancel
        </vaadin-button>
        <vaadin-button theme="primary" @click="${() => {
        if (this.role != "" && this.role != null) {
            this.dispatchEvent(new CustomEvent('edit-edge', {
                detail: {
                    role: this.role,
                    identifier: this.identifier,
                    description: this.description,
                    buffered: this.buffered
                }
            }));
        }
        this.buffered = false;
        this.close();
        this.dispatchEvent(new Event('deselect-edge'))
        }
    }"
        >Save
        </vaadin-button>
    `;

    private close() {
        this.dialogOpened = false;
    }


}
