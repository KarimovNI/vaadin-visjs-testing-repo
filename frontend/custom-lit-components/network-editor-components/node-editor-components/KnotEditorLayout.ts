import {css, html, LitElement} from 'lit';
import {customElement, property, state} from 'lit/decorators';
import '@vaadin/menu-bar';
import {createTemplate} from "Frontend/icons/get-icons";
import '@vaadin/vertical-layout';
import {applyTheme} from "Frontend/generated/theme";

import {FormLayoutResponsiveStep} from "@vaadin/form-layout";
import {Grid, GridColumn, GridDefaultItem} from "@vaadin/grid";
import {
    ItemDialog
} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/dialog-components/ItemDialog";
import {ComboBox} from "@vaadin/combo-box";
import {Identity} from "Frontend/enums/Identity";


const template = createTemplate();
document.head.appendChild(template.content);


@customElement('knot-editor-layout')
export class KnotEditorLayout extends LitElement {

    protected createRenderRoot() {
        const root = super.createRenderRoot();
        // Apply custom theme (only supported if your app uses one)
        applyTheme(root);
        return root;
    }

    static get styles() {
        return css`
        [slot="label"] {
            font-size: var(--lumo-font-size-l);
            font-family: sans-serif;     
        }
        .text-field-width{
            width: 100%;
            font-size: var(--lumo-font-size-l);
            font-family: sans-serif;   
        }
      `
    }

    @property()
    private knot: any;
    @state()
    public grid!: Grid;
    @state()
    public dialog: ItemDialog = new ItemDialog();

    @property()
    private activeItem: GridDefaultItem | null = null;
    @state()
    private selectedItems = [];
    @property()
    private deleteDisabled = true;

    setKnot(attribute: any) {
        this.knot = attribute;
    }

    setSelectedSheet(val?: number) {

    }

    getSelectedSheet() {
        return 0;
    }

    setKnots(val: any) {

    }

    createGrid() {

        let items = this.knot.values[0].value;
        this.grid = new Grid();
        this.grid.items = items;
        this.grid.style.height = '250px';
        let gridColumn;
        if (items != null && items.length != 0) {
            for (let itemKeys of Object.keys(items[0])) {
                gridColumn = new GridColumn();
                gridColumn.path = itemKeys;
                this.grid.appendChild(gridColumn);
            }
        }
        this.grid.addEventListener('active-item-changed', () => {
            this.grid.selectedItems = this.activeItem ? [this.activeItem] : []
            this.deleteDisabled = this.activeItem ? false : true

        })
        this.grid.onclick = (e: Event) => {
            if (this.activeItem == null) {
                this.activeItem = this.grid.activeItem
                this.grid.selectedItems = []
            } else if (this.activeItem != this.grid.activeItem && this.grid.activeItem != null) {
                this.activeItem = this.grid.activeItem;
                this.dispatchEvent(new CustomEvent('active-item-changed'));
            } else if (this.grid.activeItem == null) {
                this.activeItem = null;
                this.grid.selectedItems = []
            }
        };
        this.grid.ondblclick = (e: Event) => {
            if (this.activeItem != null) {
                this.dialog.itemId = this.activeItem.id;
                this.dialog.itemIdDisabled = true;
                this.dialog.value = this.activeItem.value;
                this.dialog.deprecated = this.activeItem.deprecated;
                this.dialog.dialogOpened = true;
            }
        }

    }

    private responsiveSteps: FormLayoutResponsiveStep[] = [
        {minWidth: '40px', columns: 7}
    ];
    private responsiveSteps2: FormLayoutResponsiveStep[] = [
        {minWidth: '30px', columns: 2}
    ];

    createIdentityComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.knot.identity;
        combobox.items = Object.values(Identity);
        combobox.onchange = () => {
            this.dispatchEvent(new CustomEvent("combo-box-changed", {
                detail: {
                    value: combobox.value,
                    cBoxTie: false,
                    cBoxIdentity: true
                }
            }));
        }
        combobox.label = "Identity"
        return combobox;
    }

    render() {
        this.createGrid();
        this.dialog.render();
        // @ts-ignore
        this.dialog.addEventListener('add-item', (e: Event) => {
            e.stopImmediatePropagation(); // @ts-ignore
            this.dispatchEvent(new CustomEvent('add-item-from-dialog', {detail: e.detail}))
        });
        return html`
            <vaadin-form-layout
                    style="width: 100%; padding: 20px"
                    .responsiveSteps="${this.responsiveSteps}"

            >
                <vaadin-form-layout colspan="2" .responsiveSteps="${this.responsiveSteps2}"
                                    style="padding: 0; margin: 0;">
                    <vaadin-text-field
                            label="Мнемоник"
                            class="text-field-width"
                            name="mnemonic"
                            minlength="3"
                            maxlength="3"
                            value="${this.knot.mnemonic}"
                    >
                    </vaadin-text-field>
                    <vaadin-text-field label="UID" name="uid"
                                       class="text-field-width"
                                       value="${this.knot.uid}"
                                       readonly
                    >
                    </vaadin-text-field>
                    <vaadin-text-field
                            label="Дескриптор"
                            class="text-field-width"
                            name="descriptor"
                            value="${this.knot.descriptor}"
                    >
                    </vaadin-text-field>
                    ${this.createIdentityComboBox()}

                    <vaadin-horizontal-layout colspan="2">
                        <vaadin-text-area
                                label="Описание"
                                class="text-field-width"
                                value="${this.knot.description}"
                                caret="20"
                                name="description"
                                style="height: 150px; width: 100%"
                        >
                        </vaadin-text-area>
                        <vaadin-vertical-layout theme="spacing-xs padding" style="width: 30%">
                            <vaadin-text-field 
                                    label="Length" 
                                    name="length"
                                    style="padding-top: 0; width: 100%"
                            >
                            </vaadin-text-field>
                            <vaadin-checkbox
                                    label="Deprecated"
                                    name="deprecated"
                                    .checked="${this.knot.deprecated == null ? false : this.knot.deprecated}"
                            >
                            </vaadin-checkbox>
                        </vaadin-vertical-layout>

                    </vaadin-horizontal-layout>
                    
                </vaadin-form-layout>

                <vaadin-tabsheet style="width: 100%; height: 100%" colspan="5">
                    <vaadin-tabs style="width: 98%"
                                 theme="equal-width-tabs"
                                 selected="0"
                    >
                        <vaadin-tab class="text-field-width">Values</vaadin-tab>
                    </vaadin-tabs>
                    <vaadin-vertical-layout theme="padding" style="height: 100%; width: 98%">
                        ${this.grid}
                        <vaadin-horiznotal-layout>
                            <vaadin-button @click="${() => {
                                this.dialog.itemIdDisabled = false;
                                this.dialog.dialogOpened = true
                            }}">Add item
                            </vaadin-button>
                            <vaadin-button
                                    .disabled="${this.deleteDisabled}"
                                    @click="${(e: Event) => {
                                        e.stopImmediatePropagation();
                                        this.dispatchEvent(new CustomEvent('delete-knot-item', {detail: {id: this.activeItem.id}}));
                                        this.selectedItems = [];
                                        this.activeItem = null;
                                        this.deleteDisabled = true;
                                    }}">Delete item
                            </vaadin-button>
                        </vaadin-horiznotal-layout>
                        ${this.dialog}
                    </vaadin-vertical-layout>
                </vaadin-tabsheet>


            </vaadin-form-layout>
        `;
    }
}