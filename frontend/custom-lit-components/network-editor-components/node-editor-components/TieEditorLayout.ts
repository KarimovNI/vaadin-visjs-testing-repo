import {css, html, LitElement} from 'lit';
import {customElement, property, state} from 'lit/decorators';
import '@vaadin/vertical-layout';
import {applyTheme} from "Frontend/generated/theme";
import {ComboBox} from "@vaadin/combo-box";
import {DataRange} from "Frontend/enums/DataRange"

import {FormLayoutResponsiveStep} from "@vaadin/form-layout";
import {Grid, GridColumn, GridItemModel} from "@vaadin/grid";
import {TabsSelectedChangedEvent} from "@vaadin/tabs";
import {IndexDialog} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/dialog-components/IndexDialog";
import {ColumnDialog} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/dialog-components/ColumnDialog";



@customElement('tie-editor-layout')
export class TieEditorLayout extends LitElement {

    @state()
    public grid!: Grid;
    @state()
    public dialog: IndexDialog = new IndexDialog();
    @state()
    public columnDialog: ColumnDialog = new ColumnDialog();


    @state()
    private selectedItems = [];
    @property()
    private deleteDisabled = true;

    @property()
    private selected = 1;

    @property()
    private indexButtonDisabled = 'none';
    @property()
    private columnButtonDisabled = 'flex';
    @property()
    private knots!: string[];

    setKnots(knots: string[]){
        this.knots = knots;
    }
    setSelectedSheet(sheetNumber: number){
        this.selected = sheetNumber;
    }
    getSelectedSheet(){
        return this.selected;
    }
    protected createRenderRoot() {
        const root = super.createRenderRoot();
        applyTheme(root);
        return root;
    }

    static get styles() {
        return css`
        [slot="label"] {
            font-size: var(--lumo-font-size-l);
            font-family: sans-serif;     
        }
        .text-field-width{
            width: 100%;
            font-size: var(--lumo-font-size-l);
            font-family: sans-serif;   
        }
      `
    }

    @state()
    private tie: any;

    private selectorGrid = [() => this.createIndexesGrid(), () => this.createExtendedColumnsGrid()]

    private responsiveSteps: FormLayoutResponsiveStep[] = [
        {minWidth: '40px', columns: 7}
    ];
    private responsiveSteps2: FormLayoutResponsiveStep[] = [
        {minWidth: '30px', columns: 2}
    ];

    initGridEvents() {
        this.grid.addEventListener('active-item-changed', (e: Event) => {
            // @ts-ignore
            this.grid.selectedItems = e.detail.value ? [e.detail.value] : []
            // @ts-ignore
            this.deleteDisabled = !e.detail.value;
        })

    }

    fillGrid(items: string[] | object[]) {
        let gridColumn: GridColumn<any>;
        for (let itemKey of Object.keys(items[0])) {
            gridColumn = new GridColumn();
            gridColumn.path = itemKey;
            if (itemKey == 'id' || itemKey == 'layout') {
                gridColumn.hidden = true;
            }
            this.grid.appendChild(gridColumn);
        }
        gridColumn = new GridColumn();
        gridColumn.frozenToEnd = true;
        gridColumn.autoWidth = true;
        gridColumn.flexGrow = 0;
        gridColumn.renderer = (root, column, model) => {
            let editButton = root.firstElementChild;
            if (!editButton) {
                editButton = document.createElement('vaadin-button');
                editButton.textContent = "Edit";
                editButton.addEventListener('click', (evt) => {
                    evt.stopImmediatePropagation();
                    if (Object.keys(model.item).length == 3) {
                        this.fillIndexDialog(model);
                    } else {
                        this.fillColumnDialog(model);
                    }

                });
                root.appendChild(editButton);
            }
        }
        this.grid.appendChild(gridColumn);
    }

    fillIndexDialog(model: GridItemModel<any>) {
        this.dialog.index = this.tie.indexes[0].index[this.tie.indexes[0].index.findIndex((index: { id: any; }) => index.id == model.item.id)];
        this.dialog.dialogOpened = true;
        this.dialog.indexColumns = []
        if(this.tie.anchorRole != null && this.tie.anchorRole.length != 0){
            this.dialog.indexColumns = this.dialog.indexColumns.concat(this.tie.anchorRole.map((anchor: { role: string; type: string; }) => {return anchor.type + "_" + anchor.role}));
        }
        if(this.tie.knotRole != null && this.tie.knotRole.length != 0){
            this.dialog.indexColumns = this.dialog.indexColumns.concat(this.tie.knotRole.map((knot: { role: string; type: string; }) => {return knot.type +"_"+ knot.role}));
        }
        if(this.tie.extendedColumn != null){
            this.dialog.indexColumns = this.dialog.indexColumns.concat(this.tie.extendedColumn.map((column: { name: string; }) => {
                return column.name
            }));
        }
        if(this.tie.timeRange != null && this.tie.timeRange != ""){
            // @ts-ignore
            this.dialog.indexColumns.push("changedat");
        }
    }

    fillColumnDialog(model: GridItemModel<any>) {
        this.columnDialog.column = structuredClone(model.item);
        this.columnDialog.knots = this.knots;
        this.columnDialog.dialogOpened = true;
    }

    createIndexesGrid() {
        this.grid = new Grid();
        this.grid.style.height = '250px';
        this.grid.style.width = '100%';
        if (this.tie.indexes != null) {
            let attribute = this.tie;
            let items = [];
            for (let index of attribute.indexes[0].index) {
                items.push(
                    {
                        id: index['id'],
                        columns: index.columns.column.join(),
                        type: index.type
                    }
                )
            }
            this.grid.items = items;
            if (items.length != 0) {
                this.fillGrid(items);
            }
        }
        this.initGridEvents();
    }

    createExtendedColumnsGrid() {
        this.grid = new Grid();
        this.grid.style.height = '250px';
        this.grid.style.width = '100%';
        if (this.tie.extendedColumn != null && this.tie.extendedColumn.length != 0) {
            let items = this.tie.extendedColumn;
            this.grid.items = items;
            if (items.length != 0) {
                this.fillGrid(items);
            }
        }
        this.initGridEvents();
    }

    joinColumns(index: any) {
        if (typeof index.columns.column != 'undefined') {
            index.columns = index.columns.column.join(', ');
        }
        return index;
    }

    setTie(tie: any) {
        this.tie = tie;
    }

    createComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.tie.timeRange;
        combobox.items = ['DATETIME', 'BIGINT'];
        combobox.onchange = () => {
            this.dispatchEvent(new CustomEvent("combo-box-changed", {detail: {value: combobox.value, cBoxTie: true}}));
        }
        combobox.label = "Time Range"
        return combobox;
    }

    selectedChanged(e: TabsSelectedChangedEvent) {
        this.deleteDisabled = true;
        this.selectorGrid[e.detail.value]();
        this.selected = e.detail.value;
        if (e.detail.value != 0) {
            this.indexButtonDisabled = 'none';
            this.columnButtonDisabled = 'flex';
        } else {
            this.indexButtonDisabled = 'flex';
            this.columnButtonDisabled = 'none';
        }
    }
    createDataRangeComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.tie.dataRange;
        combobox.items = Object.values(DataRange);
        combobox.onchange = () => {
            this.dispatchEvent(new CustomEvent("combo-box-changed", {detail: {value: combobox.value, cBoxTie: false, cBoxDataRange: true}}));
        }
        combobox.label = "Data range"
        return combobox;
    }

    render() {

        this.dialog.render();
        this.dialog.addEventListener('add-index', (e: Event) => {
            e.stopImmediatePropagation();
            // @ts-ignore
            if (e.detail.columns.column.length == 0) {
                // @ts-ignore
                this.dispatchEvent(new CustomEvent('delete-index', {detail: {id: Number(e.detail.id)}}));
            } else {
                // @ts-ignore
                this.dispatchEvent(new CustomEvent('add-index-from-dialog', {detail: e.detail}));
            }
            this.selected = 0;
            this.createIndexesGrid();
            this.dialog = new IndexDialog();
        });
        this.columnDialog.render();
        this.columnDialog.addEventListener('add-column', (e: Event) => {
            e.stopImmediatePropagation();
            // @ts-ignore
            this.dispatchEvent(new CustomEvent('add-column-from-dialog', {detail: e.detail}));
            this.createExtendedColumnsGrid();
            this.columnDialog = new ColumnDialog();
        });
        return html`
            <vaadin-form-layout style="width: 100%; padding: 20px"
                                .responsiveSteps="${this.responsiveSteps}">
                <vaadin-form-layout colspan="2" .responsiveSteps="${this.responsiveSteps2}"
                                    style="padding: 0; margin: 0;">
                    <vaadin-text-field
                            label="Дескриптор"
                            class="text-field-width"
                            name="descriptor"
                            value="${this.tie.descriptor}"
                    >
                    </vaadin-text-field>
                    <vaadin-text-field label="UID" name="uid"
                                       class="text-field-width"
                                       value="${this.tie.uid}"
                                       readonly
                    >
                    </vaadin-text-field>
                    <vaadin-text-field
                            label="KnotRole"
                            name="knotRole"
                            class="text-field-width"
                            value="${this.tie.knotRole != null && this.tie.knotRole.length != 0 ? this.tie.knotRole[0].type : null}"
                            readonly
                    >
                    </vaadin-text-field>
                    ${this.createComboBox()}
                    <vaadin-horizontal-layout colspan="2">
                        <vaadin-text-area
                                label="Описание"
                                class="text-field-width"
                                value="${this.tie.description}"
                                caret="20"
                                name="description"
                                style="height: 150px"
                        >
                        </vaadin-text-area>
                        <vaadin-vertical-layout theme="spacing-xs padding" style="width: 30%">
                            <br>
                            <vaadin-checkbox
                                    name="timeRange"
                                    label="Исторический"
                                    .checked="${this.tie.timeRange != null && this.tie.timeRange != ""}"
                            >
                            </vaadin-checkbox>
                            <vaadin-checkbox
                                    label="Кнотированный"
                                    name="knotRole"
                                    .checked="${this.tie.knotRole != null}"
                            >
                            </vaadin-checkbox>
                            <vaadin-checkbox
                                    label="Deprecated"
                                    name="deprecated"
                                    .checked="${this.tie.deprecated == null ? false : this.tie.deprecated}"
                            >
                            </vaadin-checkbox>
                        </vaadin-vertical-layout>
                    </vaadin-horizontal-layout>
                </vaadin-form-layout>
                <vaadin-tabsheet style="width: 100%" colspan="5">
                    <vaadin-tabs @selected-changed="${this.selectedChanged}"
                                 style="width: 98%"
                                 theme="equal-width-tabs"
                                 selected="${this.selected}"
                    >
                        <vaadin-tab class="text-field-width">Индексы</vaadin-tab>
                        <vaadin-tab class="text-field-width">Extended columns</vaadin-tab>
                    </vaadin-tabs>
                    <vaadin-vertical-layout theme="padding" style="width: 98%; height: 100%">
                        ${this.grid}
                        <vaadin-horizontal-layout>
                            <vaadin-button 
                                    @click="${() => {
                                        this.dialog.dialogOpened = true;
                                        this.dialog.indexColumns = [];
                                        if(this.tie.anchorRole != null && this.tie.anchorRole.length != 0){
                                            this.dialog.indexColumns = this.dialog.indexColumns.concat(this.tie.anchorRole.map((anchor: { role: string; type: string; }) => {return anchor.type + "_" + anchor.role}));
                                        }
                                        if(this.tie.knotRole != null && this.tie.knotRole.length != 0){
                                            this.dialog.indexColumns = this.dialog.indexColumns.concat(this.tie.knotRole.map((knot: { role: string; type: string; }) => {return knot.type +"_"+ knot.role}));
                                        } 
                                        
                                        if(this.tie.extendedColumn != null){
                                            this.dialog.indexColumns = this.dialog.indexColumns.concat(this.tie.extendedColumn.map((column: { name: string; }) => {
                                                return column.name
                                            }));
                                        }
                                        if(this.tie.timeRange != null && this.tie.timeRange != ""){
                                            // @ts-ignore
                                            this.dialog.indexColumns.push("changedat");
                                        }
                                    }}" 
                                           style="display: ${this.indexButtonDisabled}">Add index
                            </vaadin-button>
                            <vaadin-button
                                    @click="${() => {
                                        this.columnDialog.dialogOpened = true; 
                                        this.columnDialog.knots = this.knots;
                                    }}"
                                    style="display:${this.columnButtonDisabled}"
                            >Add column
                            </vaadin-button>
                            <vaadin-button
                                    .disabled="${this.deleteDisabled}"
                                    style="display: ${this.indexButtonDisabled}"
                                    @click="${(e: Event) => {
                                        e.stopImmediatePropagation();
                                        this.dispatchEvent(new CustomEvent('delete-index', {detail: {id: this.grid.activeItem.id}}));
                                        this.selectedItems = [];
                                        this.deleteDisabled = true;
                                        this.createIndexesGrid();
                                        this.selected = 0;
                                    }}"
                            >Delete index
                            </vaadin-button>
                            <vaadin-button
                                    .disabled="${this.deleteDisabled}"
                                    style="display: ${this.columnButtonDisabled}"
                                    @click="${(e: Event) => {
                                        e.stopImmediatePropagation();
                                        this.dispatchEvent(new CustomEvent('delete-column', {detail: {id: this.grid.activeItem.id}}));
                                        this.selectedItems = [];
                                        this.deleteDisabled = true;
                                        this.createExtendedColumnsGrid();
                                        this.selected = 1;
                                    }}"
                            >Delete column
                            </vaadin-button>
                        </vaadin-horizontal-layout>
                        ${this.dialog}
                        ${this.columnDialog}
                    </vaadin-vertical-layout>
                </vaadin-tabsheet>
            </vaadin-form-layout>
        `;
    }
}