// @ts-nocheck

import {html, LitElement} from 'lit';
import {customElement, state} from 'lit/decorators.js';

import '@vaadin/button';
import '@vaadin/dialog';
import '@vaadin/text-field';
import '@vaadin/vertical-layout';
import {dialogFooterRenderer, dialogRenderer} from '@vaadin/dialog/lit.js';
import type {DialogOpenedChangedEvent} from '@vaadin/dialog';
import {applyTheme} from 'Frontend/generated/theme';
import {property} from "lit/decorators";
import {ComboBox} from "@vaadin/combo-box";
import {ExtendedColumn} from "Frontend/interfaces/Interfaces";
import {DataRange} from "Frontend/enums/DataRange";
import {FormLayoutResponsiveStep} from "@vaadin/form-layout";

@customElement('dialog-column')
export class ColumnDialog extends LitElement {
    protected createRenderRoot() {
        const root = super.createRenderRoot();
        // Apply custom theme (only supported if your app uses one)
        applyTheme(root);
        return root;
    }

    @state()
    public dialogOpened = false;

    private columnTemplate: ExtendedColumn  = {
        id: null,
        description: null,
        descriptor: null,
        uid: null, // на чтение
        deprecated: null,
        name: null,
        knotRange: null, // knotRange исключает dataRange указывается абсолютно любой кнот
        dataRange: null, // string, bigint, clob, blob
        length: 512, // 512
        layout: null
    };

    @property()
    public column: ExtendedColumn | null = this.columnTemplate;

    public columnId: number | null = null;

    @property()
    private dataRangeComboBox: ComboBox;
    @property()
    private knotRangeComboBox: ComboBox;

    @property()
    public knots: string[];

    createDataRangeComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.column.dataRange;
        combobox.items = Object.values(DataRange);
        combobox.onchange = (e: Event) => {
            if(e.target.value != null && e.target.value != ""){
                this.column.dataRange = e.target.value;
                this.column.knotRange = null;
            } else {
                this.column.dataRange = null;
            }
            this.knotRangeComboBox.value = this.column.knotRange;
        }
        combobox.label = "Data range"
        combobox.setAttribute("colspan", "2");
        return combobox;
    }
    createKnotRangeComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.column.knotRange;
        combobox.items = this.knots;
        combobox.onchange = (e: Event) => {
            if(e.target.value != null && e.target.value != ""){
                this.column.knotRange = e.target.value
                this.column.dataRange = null;
            } else{
                this.column.knotRange = null;
            }
            this.dataRangeComboBox.value = this.column.dataRange;
        }
        combobox.label = "Knot range";
        combobox.setAttribute("colspan", "1");
        return combobox;
    }
    render() {
        return html`
            <vaadin-dialog
                    header-title="New column"
                    .opened="${this.dialogOpened}"
                    @opened-changed="${(e: DialogOpenedChangedEvent) => (this.dialogOpened = e.detail.value)}"
                    ${dialogRenderer(this.renderDialog, [])}
                    ${dialogFooterRenderer(this.renderFooter, [])}
            ></vaadin-dialog>
        `;
    }
    private responsiveSteps: FormLayoutResponsiveStep[] = [
        {minWidth: '50px', columns: 2}
    ];
    // @ts-ignore
    private renderDialog = () => {

        if(this.column == null){
            this.column = this.columnTemplate;
        }

        this.dataRangeComboBox = this.createDataRangeComboBox();
        this.knotRangeComboBox = this.createKnotRangeComboBox();

        return html`
        <vaadin-form-layout style="align-items: stretch; width: 18rem; max-width: 100%;" .responsiveSteps="${this.responsiveSteps}">
            <vaadin-text-field label="UID" readonly
                               value="${this.column.uid}" colspan="2"></vaadin-text-field>
            <vaadin-text-field label="Name" @input="${(e: Event) => this.column.name = e.target!.value}"
                               value="${this.column.name}" colspan="2"></vaadin-text-field>
            ${this.knotRangeComboBox}
            <vaadin-checkbox label="Deprecated" @input="${(e: Event) => {this.column.deprecated= e.target!.checked;}}"
                             value="${this.column.deprecated}" colspan="1"></vaadin-checkbox>
            <vaadin-text-field label="Length" @input="${(e: Event) => this.column.length = e.target!.value}"
                               value="${this.column.length}" colspan="2"></vaadin-text-field>
            ${this.dataRangeComboBox}
            <vaadin-text-field label="Descriptor" @input="${(e: Event) => this.column.descriptor = e.target!.value}"
                               value="${this.column.descriptor}" colspan="2"></vaadin-text-field>
            <vaadin-text-area label="Description" @input="${(e: Event) => this.column.description = e.target!.value}"
                              value="${this.column.description}" colspan="2"></vaadin-text-area>

        </vaadin-form-layout>
    `;
    }

    private renderFooter = () => html`
        <vaadin-button @click="${() => {
            this.close();
        }}">Cancel
        </vaadin-button>
        <vaadin-button theme="primary" @click="${() => {
            if (this.column.name != "" && this.column.name != null &&
                ((this.column.knotRange != null && this.column.knotRange != "") 
                    ||(this.column.dataRange != null && this.column.dataRange != ""))
            ) {
                this.dispatchEvent(new CustomEvent('add-column', {
                    detail: {column: this.column}
                }));
            }
            this.close();
        }}"
        >Add
        </vaadin-button>
    `;

    private close() {
        this.column = structuredClone(this.columnTemplate);
        this.dialogOpened = false;
    }


}
