import {html, LitElement} from 'lit';
import {customElement, state} from 'lit/decorators.js';

import '@vaadin/button';
import '@vaadin/dialog';
import '@vaadin/text-field';
import '@vaadin/vertical-layout';
import {dialogFooterRenderer, dialogRenderer} from '@vaadin/dialog/lit.js';
import type {DialogOpenedChangedEvent} from '@vaadin/dialog';
import {applyTheme} from 'Frontend/generated/theme';
import {property} from "lit/decorators";
import {ComboBox} from "@vaadin/combo-box";
import {FormLayoutResponsiveStep} from "@vaadin/form-layout";
import {Grid, GridColumn} from "@vaadin/grid";

import {IndexType} from "Frontend/enums/IndexType";
import {Button} from "@vaadin/button";
import {Checkbox} from "@vaadin/checkbox";

@customElement('dialog-index')
export class IndexDialog extends LitElement {
    protected createRenderRoot() {
        const root = super.createRenderRoot();
        // Apply custom theme (only supported if your app uses one)
        applyTheme(root);
        return root;
    }

    @state()
    public dialogOpened = false;

    @state()
    private grid!: Grid;

    private indexTemplate: {
        method: string | null,
        id: string | number | null,
        type: string | null,
        columns: { column: string[] }
    } = {
        id: null,
        type: null,
        columns: {
            column: []
        },
        method: null
    };

    @property()
    public index = this.indexTemplate;

    createGrid() {
        this.grid = new Grid();
        this.grid.style.height = '250px';
        this.grid.style.width = '100%';

        this.grid.setAttribute('colspan', '3');

        // @ts-ignore
        this.grid.items = this.index.columns.column.map((column: string) => {
            return {column: column.split(" ")[0], order: column.split(" ")[1] ? column.split(" ")[1] : "ASC"}
        });

        // this..items = this.indexColumns.filter(x => !gridItems.includes(x));

        let columnPath = new GridColumn();
        columnPath.path = 'column';
        this.grid.appendChild(columnPath);

        let orderPath = new GridColumn();
        orderPath.path = 'order';
        this.grid.appendChild(orderPath);

        this.grid.addEventListener('active-item-changed', (e: Event) => {
            // @ts-ignore
            this.grid.selectedItems = e.detail.value ? [e.detail.value] : []
            if (this.grid.activeItem != null) {
                if (this.grid.activeItem.order == null) {
                    this.orderComboBox.value = 'ASC'
                    this.columnComboBox.value = this.grid.activeItem.column;
                }
                if (this.grid.activeItem.order != null) {
                    this.orderComboBox.value = this.grid.activeItem.order.toUpperCase();
                    this.columnComboBox.value = this.grid.activeItem.column;
                }
            } else {
                // @ts-ignore
                this.orderComboBox.value = null
                // @ts-ignore
                this.columnComboBox.value = null;
            }
            // @ts-ignore
            this.deleteButton.disabled = !e.detail.value;
        });
    }

    @state()
    private orderValue = 'ASC';
    @property()
    private typeValue: string = "ordinary";
    @property()
    private method = null;
    @property()
    public columns: string = "";
    @property()
    public indexColumns = [];
    @property()
    private selectedColumnName: string | null = null;

    @property()
    private unique = false;
    @state()
    private uniqueCheckBox!: Checkbox;

    @state()
    private orderComboBox!: ComboBox;
    @state()
    private columnComboBox!: ComboBox;
    @state()
    private methodComboBox!: ComboBox;
    @state()
    private deleteButton!: Button;

    createDeleteButton() {
        let button = new Button();
        button.textContent = 'Delete';
        button.onclick = () => {
            let items = this.grid.items;
            items!.splice(items!.findIndex(o => o.column == this.grid.activeItem.column), 1)
            this.grid.items = []
            this.grid.items = items;
            this.grid.selectedItems = [];
            this.grid.activeItem = null;
        }
        button.disabled = true;
        return button;
    }

    createMethodComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.method ? this.method : IndexType.B_TREE;
        combobox.items = Object.values(IndexType);
        // combobox.onchange = () => {
        //     this.dispatchEvent(new CustomEvent("combo-box-changed", {detail: {value: combobox.value, cBoxTie: true}}));
        // }
        combobox.label = "Index type"
        return combobox;
    }


    createOrderComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.orderValue;
        combobox.items = ['ASC', 'DESC'];
        // combobox.onchange = () => {
        //     this.dispatchEvent(new CustomEvent("combo-box-changed", {detail: {value: combobox.value, cBoxTie: true}}));
        // }
        combobox.label = "Index order"
        return combobox;
    }

    createColumnNameComboBox() {
        let combobox = new ComboBox();
        combobox.className = "text-field-width";
        combobox.value = this.selectedColumnName!;
        combobox.items = this.indexColumns;
        // combobox.onchange = () => {
        //     this.dispatchEvent(new CustomEvent("combo-box-changed", {detail: {value: combobox.value, cBoxTie: true}}));
        // }
        combobox.label = "Column"
        return combobox;
    }

    render() {
        // @ts-ignore
        this.typeValue = this.index.type;
        // @ts-ignore
        this.method = this.index.method ? this.index.method : IndexType.B_TREE;
        this.methodComboBox = this.createMethodComboBox();
        this.orderComboBox = this.createOrderComboBox();
        this.columnComboBox = this.createColumnNameComboBox();
        this.deleteButton = this.createDeleteButton();
        this.createGrid();
        this.createUniqueCheckBox();
        // this.renderedColumns = this.index.map((column:string) => {return {column: column.split(" ")[0], order: column.split(" ")[1]}});
        return html`
            <vaadin-dialog
                    header-title="Index"
                    .opened="${this.dialogOpened}"
                    @opened-changed="${(e: DialogOpenedChangedEvent) => (this.dialogOpened = e.detail.value)}"
                    ${dialogRenderer(this.renderDialog, [])}
                    ${dialogFooterRenderer(this.renderFooter, [])}
            ></vaadin-dialog>
        `;
    }

    private responsiveSteps: FormLayoutResponsiveStep[] = [
        {minWidth: '50px', columns: 3}
    ];

    createUniqueCheckBox() {
        this.uniqueCheckBox = new Checkbox();
        this.uniqueCheckBox.name = 'Unique';
        this.uniqueCheckBox.label = 'Unique';
        this.uniqueCheckBox.checked = this.typeValue ? this.typeValue.toLowerCase() == 'unique' : false;
        this.uniqueCheckBox.setAttribute('colspan', '2');
    }

    private renderDialog = () => html`
        <vaadin-form-layout style="align-items: stretch; width: 25rem; max-width: 100%;"
                            .responsiveSteps="${this.responsiveSteps}">
            ${this.methodComboBox}
            ${this.uniqueCheckBox}
            <hr colspan="3"/>
            ${this.columnComboBox}
            ${this.orderComboBox}
            <vaadin-button
                    theme="secondary"
                    style="width: 10px"
                    @click="${() => {
                        if (this.columnComboBox.value != "" && this.columnComboBox.value != null) {
                            if (this.grid.items?.findIndex(o => o.column == this.columnComboBox.value) != -1) {
                                this.grid.items?.splice(this.grid.items?.findIndex(o => o.column == this.columnComboBox.value), 1);
                            }
                            this.grid.items = this.grid.items?.concat([{
                                column: this.columnComboBox.value,
                                order: this.orderComboBox.value ? this.orderComboBox.value : 'ASC'
                            }])
                        }
                    }}"
            >
                Add column
            </vaadin-button>
            <hr colspan="3"/>
            ${this.grid}


        </vaadin-form-layout>
    `;

    private renderFooter = () => html`
        ${this.deleteButton}
        <vaadin-button @click="${() => {
            this.close();
            this.index = this.indexTemplate;
            this.dialogOpened = false;
        }}">Cancel
        </vaadin-button>
        <vaadin-button theme="primary" @click="${() => {
            this.index.type = this.uniqueCheckBox.checked ? 'UNIQUE' : 'ORDINARY';
            this.index.method = this.methodComboBox.value ? this.methodComboBox.value : IndexType.B_TREE;
            this.index.columns.column = this.grid.items!.map(o => {
                return o.column + " " + o.order
            })
            this.dispatchEvent(new CustomEvent('add-index', {
                detail: this.index
            }));
            this.close();
            this.index = this.indexTemplate;
            this.dialogOpened = false;
        }}"
        >Add
        </vaadin-button>
    `;

    private close() {
        this.dialogOpened = false;
    }


}
