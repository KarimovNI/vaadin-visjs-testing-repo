// @ts-nocheck

import {css, html, LitElement} from 'lit';
import {customElement, state} from 'lit/decorators.js';

import '@vaadin/button';
import '@vaadin/dialog';
import '@vaadin/text-field';
import '@vaadin/vertical-layout';
import {dialogFooterRenderer, dialogRenderer} from '@vaadin/dialog/lit.js';
import type {DialogOpenedChangedEvent} from '@vaadin/dialog';
import {applyTheme} from 'Frontend/generated/theme';
import {property} from "lit/decorators";

@customElement('dialog-item')
export class ItemDialog extends LitElement {
    protected createRenderRoot() {
        const root = super.createRenderRoot();
        // Apply custom theme (only supported if your app uses one)
        applyTheme(root);
        return root;
    }

    @state()
    public dialogOpened = false;

    @property()
    public itemIdDisabled = false;
    @property()
    public value: string = "";
    @property()
    public deprecated: boolean = false;
    public itemId: number | null = null;

    render() {
        return html`
            <vaadin-dialog
                    header-title="New item"
                    .opened="${this.dialogOpened}"
                    @opened-changed="${(e: DialogOpenedChangedEvent) => (this.dialogOpened = e.detail.value)}"
                    ${dialogRenderer(this.renderDialog, [])}
                    ${dialogFooterRenderer(this.renderFooter, [])}
            ></vaadin-dialog>
        `;
    }

    private renderDialog = () => html`
        <vaadin-vertical-layout style="align-items: stretch; width: 18rem; max-width: 100%;">
            <vaadin-text-field label="Value" @input="${(e: Event) => this.value = e.target!.value}"
                               value="${this.value}"></vaadin-text-field>
            <vaadin-text-field label="Id" @input="${(e: Event) => this.itemId = e.target!.value}"
                               value="${this.itemId}" .disabled="${this.itemIdDisabled}"></vaadin-text-field>
        </vaadin-vertical-layout>
    `;

    private renderFooter = () => html`
        <vaadin-button @click="${() => {
            this.close();
            this.itemId = null;
        }}">Cancel
        </vaadin-button>
        <vaadin-button theme="primary" @click="${() => {
            if (this.value != null && this.value != "") {
                this.dispatchEvent(new CustomEvent('add-item', {
                    detail: {
                        id: this.itemId,
                        value: this.value
                    }
                }));
            }
            this.itemId = null;
            this.close();
        }}"
        >Add
        </vaadin-button>
    `;

    private close() {
        this.dialogOpened = false;
    }


}
