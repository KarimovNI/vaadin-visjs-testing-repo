export enum DataRange {
    BIGINT = 'BIGINT',
    STRING = 'STRING',
    // CLOB = 'CLOB',
    // BLOB = 'BLOB',
    DATE = 'DATE',
    TIME='TIME'
}