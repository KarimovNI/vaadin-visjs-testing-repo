export enum NodeType{
  NO_TYPE = 0,

  ANCHOR = 1,
  TIE = 2,
  KNOT = 3,
  ATTRIBUTE = 4,

  TX_ANCHOR = 5,

  KNOT_AND_OTHERS = 6,

  ANCHORS_GROUP= 7,

  ANCHOR_AND_TIE = 8
}