export interface Metadata {
    privacy?: any;
    capsule: string;
    restatable?: any;
    generator: boolean;
    idempotent?: any;
    deletable?: any;
}

export interface InnerColumn {
    columnName: string;
    description: string;
    dataRange: string;
    logicalType?: any;
    default?: any;
}

export interface Columns {
    column: string[];
}

export interface Index {
    type: string;
    columns: Columns;
    method: string;
}

export interface Indexes {
    index: Index[];
}

export interface Layout {
    x: number;
    y: number;
    fixed: boolean;
}

export interface ExtendedColumn {
    layout?: Layout; // не надо
    description?: string;
    descriptor: string;
    uid?: any; // на чтение
    deprecated: boolean;
    name: string;
    knotRange: string; // knotRange исключает dataRange указывается абсолютно любой кнот
    dataRange: string; // string, bigint, clob, blob
    length?: any; // 512
}

export interface Attribute {
    layout: Layout;
    description: string;
    descriptor: string;
    uid?: string;
    deprecated: boolean;
    extendedColumn: ExtendedColumn[];
    timeRange: string;
    knotRange: string;
    mnemonic: string;
    dataRange: string;
    length?: number;
    indexes: Indexes | null;
}

export interface Anchor {
    layout: Layout;
    description: string;
    descriptor: string;
    uid?: any;
    deprecated: boolean;
    verticalPropertyGroup: any[];
    extendedColumn: ExtendedColumn[];
    attribute: Attribute[];
    mnemonic: string;
    identity: string;
    skip: boolean;
    indexes: Indexes | null;
}

export interface Value {
    id: number;
    value: string;
}

export interface Values {
    value: Value[];
}

export interface Knot {
    layout: Layout;
    description: string;
    descriptor: string;
    uid?: any;
    deprecated: boolean;
    values: Values;
    mnemonic: string;
    length?: any;
    identity: string;
}


export interface AnchorRole {
    description: string;
    role: string;
    type: string;
    identifier: boolean;
}

export interface KnotRole {
    description: string;
    role: string;
    type: string;
    identifier: boolean;
}

export interface Tie {
    layout: Layout;
    description: string;
    descriptor: string;
    uid?: any;
    deprecated: boolean;
    extendedColumn?: ExtendedColumn[];
    anchorRole: AnchorRole[];
    knotRole: KnotRole | null;
    timeRange: string;
    indexes: Indexes | null;
}

export interface TxAnchor {
    attribute: Attribute[];
    mnemonic: string;
    identity: string;
    layout?: Layout;
    description: string;
    descriptor?: string;
    uid?: any;
    deprecated: boolean;
    extendedColumn?: ExtendedColumn[];
    anchorRole: AnchorRole[];
    indexes: Indexes | null;
}