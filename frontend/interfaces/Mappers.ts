import {
    Anchor,
    AnchorRole,
    Attribute,
    ExtendedColumn,
    InnerColumn,
    Knot,
    Tie,
    TxAnchor
} from "Frontend/interfaces/Interfaces";

export const mapNodeToAttribute = (node: any) => {
    let attribute: Attribute = {
        descriptor: node.descriptor,
        uid: node.uid,
        deprecated: node.deprecated,
        description: node.description,
        mnemonic: node.mnemonic,
        dataRange: node.dataRange,
        timeRange: node.timeRange,
        knotRange: node.knotRange,
        length: node.length,
        extendedColumn: node.extendedColumn != null && node.extendedColumn.length != 0 ? node.extendedColumn.map(
            (column: ExtendedColumn) => {
                return {
                    layout: null,
                    description: column.description,
                    descriptor: column.descriptor,
                    uid: column.uid,
                    deprecated: column.deprecated,
                    name: column.name,
                    knotRange: column.knotRange,
                    dataRange: column.dataRange,
                    length: column.length
                }
            }
        ) : [],
        layout: {
            x: node.x.toFixed(2),
            y: node.y.toFixed(2),
            fixed: node.fixed,
        },
        indexes: node.indexes != null && node.indexes.length != 0 ? {
            index:
                node.indexes[0].index.map((index: any) => {
                    return {
                        type: index.type,
                        columns: {
                            column: index.columns.column
                        },
                        method: index.method
                    }
                })
        } : null
    };

    return attribute;
}

export const mapNodeToAnchor = (node: any) => {
    let anchor: Anchor = {
        uid: node.uid,
        attribute: node.attribute != null && node.attribute.length != 0 ? node.attribute.map((attr: any) => mapNodeToAttribute(attr)) : [],
        verticalPropertyGroup: node.verticalPropertyGroup,
        descriptor: node.descriptor,
        deprecated: node.deprecated,
        description: node.description,
        mnemonic: node.mnemonic,
        identity: node.identity,
        skip: node.skip,
        extendedColumn: node.extendedColumn != null && node.extendedColumn.length != 0 ? node.extendedColumn.map(
            (column: ExtendedColumn) => {
                return {
                    layout: null,
                    description: column.description,
                    descriptor: column.descriptor,
                    uid: column.uid,
                    deprecated: column.deprecated,
                    name: column.name,
                    knotRange: column.knotRange,
                    dataRange: column.dataRange,
                    length: column.length
                }
            }
        ) : [],
        layout: {
            x: node.x.toFixed(2),
            y: node.y.toFixed(2),
            fixed: node.fixed,
        },
        indexes: node.indexes != null && node.indexes.length != 0 ? {
            index:
                node.indexes[0].index.map((index: any) => {
                    return {
                        type: index.type,
                        columns: {
                            column: index.columns.column
                        },
                        method: index.method
                    }
                })
        } : null
    };
    return anchor;
    // return {
    //     "oldMnemonic": node.oldMnemonic,
    //     "anchor": anchor
    // };
}

export const mapNodeToKnot = (node: any) => {
    let knot: Knot = {
        uid: node.uid,
        layout: {
            x: node.x.toFixed(2),
            y: node.y.toFixed(2),
            fixed: node.fixed,
        },
        values: {
            value: node.values[0].value
        },
        descriptor: node.descriptor,
        deprecated: node.deprecated,
        description: node.description,
        mnemonic: node.mnemonic,
        identity: node.identity
    };
    return knot;
    // return {
    //     "oldMnemonic": node.oldMnemonic,
    //     "knot": knot
    // };
}

export const mapNodeToTie = (node: any) => {
    let tie: Tie = {
        uid: node.uid,
        deprecated: node.deprecated,
        description: node.description,
        descriptor: node.descriptor,
        knotRole: node.knotRole != null && node.knotRole.length != 0 ? {
            description: node.knotRole[0].description,
            role: node.knotRole[0].role,
            type: node.knotRole[0].type,
            identifier: node.knotRole[0].identifier
        } : null,
        layout: {
            x: node.x.toFixed(2),
            y: node.y.toFixed(2),
            fixed: node.fixed,
        },
        timeRange: node.timeRange,
        anchorRole: node.anchorRole != null && node.anchorRole.length != 0 ? node.anchorRole.map((anchor: AnchorRole) => {
            return {
                role: anchor.role,
                type: anchor.type,
                identifier: anchor.identifier,
                description: anchor.description
            }
        }) : null,
        extendedColumn: node.extendedColumn != null && node.extendedColumn.length != 0 ? node.extendedColumn.map(
            (column: ExtendedColumn) => {
                return {
                    layout: null,
                    description: column.description,
                    descriptor: column.descriptor,
                    uid: column.uid,
                    deprecated: column.deprecated,
                    name: column.name,
                    knotRange: column.knotRange,
                    dataRange: column.dataRange,
                    length: column.length
                }
            }
        ) : [],
        indexes: node.indexes != null && node.indexes.length != 0 ? {
            index:
                node.indexes[0].index.map((index: any) => {
                    return {
                        type: index.type,
                        columns: {
                            column: index.columns.column
                        },
                        method: index.method
                    }
                })
        } : null
    }
    return tie;
}

export const mapNodeToTxAnchor = (node: any) => {
    let txAnchor: TxAnchor = {
        attribute: node.attribute != null && node.attribute.length != 0 ? node.attribute.map((attr: any) => mapNodeToAttribute(attr)) : [],
        mnemonic: node.mnemonic,
        identity: node.identity,
        uid: node.uid,
        anchorRole: node.anchorRole != null && node.anchorRole.length != 0 ? node.anchorRole.map((anchor: AnchorRole) => {
            return {
                role: anchor.role,
                type: anchor.type,
                identifier: anchor.identifier,
                description: anchor.description
            }
        }) : null,
        deprecated: node.deprecated,
        description: node.description,
        descriptor: node.descriptor,
        extendedColumn: node.extendedColumn != null && node.extendedColumn.length != 0 ? node.extendedColumn.map(
            (column: ExtendedColumn) => {
                return {
                    layout: null,
                    description: column.description,
                    descriptor: column.descriptor,
                    uid: column.uid,
                    deprecated: column.deprecated,
                    name: column.name,
                    knotRange: column.knotRange,
                    dataRange: column.dataRange,
                    length: column.length
                }
            }
        ) : [],
        layout: {
            x: node.x.toFixed(2),
            y: node.y.toFixed(2),
            fixed: node.fixed,
        },
        indexes: node.indexes != null && node.indexes.length != 0 ? {
            index:
                node.indexes[0].index.map((index: any) => {
                    return {
                        type: index.type,
                        columns: {
                            column: index.columns.column
                        },
                        method: index.method
                    }
                })
        } : null

    }
    return txAnchor;
}