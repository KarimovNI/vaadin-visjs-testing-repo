import {DomainRenderer} from "Frontend/renderer/DomainRenderer";
import {NodeType} from "Frontend/enums/NodeType";
import {randomBytes} from "crypto";
import {Network, Node} from "vis-network";
import {uuid} from "Frontend/uuid/uuid";
const domainRenderer = new DomainRenderer();

export const parseNodeMainData = (lastId: number | string, indexId: number, node: object | any, parentNode?: object | any) => {
    if (node['mnemonic'] == null) {
        node['id'] = lastId.toString();
        node['_id'] = null;
        node['mnemonic'] = lastId.toString();
    } else {
        node['_id'] = node['id'];
        if (parentNode != null) {
            node['id'] = parentNode['mnemonic'] + "_" + node['mnemonic'];
        } else {
            node['id'] = node['mnemonic']
        }
        // node['oldMnemonic'] = node['mnemonic'];
    }
    node['label'] = node['descriptor'];
    if (node['indexes'] != null) {
        for (let indexNum = 0; indexNum < node['indexes'].index.length; indexNum++) {
            node['indexes'].index[indexNum]['id'] = indexId++;
        }
        node['indexes'] = [node['indexes']]
    } else {
        node['indexes'] = null;
    }
    if (node['values'] != null) {
        node['values'] = [node['values']]
    }
    node['extendedColumn'] = node['extendedColumn'] ? node['extendedColumn'] : [];
    if (node['extendedColumn'] != null && node['extendedColumn'].length != 0) {
        node['extendedColumn'] = node['extendedColumn'].map((column: any) => addId(column, indexId));
    }
    return node;
}

export const parseNodeLayout = (node: object | any, motherNode?: object | any) => {
    if (motherNode != null && motherNode['layout'] != null) {
        node['fixed'] = false;
        node['x'] = motherNode['layout']["x"];
        node['y'] = motherNode['layout']["y"];
    } else {
        if (node['layout'] != null) {
            if (node['layout']["fixed"] === true) {
                node['fixed'] = true;
            } else {
                node['fixed'] = false;
            }
            node['x'] = node['layout']["x"];
            node['y'] = node['layout']["y"];
        } else {
            node['fixed'] = false;
            node['x'] = 700;
            node['y'] = 700;
        }
    }
    return node;
}

export const fillAnchorFigure = (node: object | any) => {
    node['_group'] = node['group'];
    delete node['group'];
    node['type'] = NodeType.ANCHOR;

    // for(let attr of node['attribute']){
    //     if(attr.columnName != null && attr.columnName != ""){
    //         node['transactional'] = true;
    //         break;
    //     }
    // }

    node = domainRenderer.renderAnchor(node);

    return node;
}

export const fillTxAnchorFigure = (node: object | any) => {
    node['_group'] = node['group'];
    delete node['group'];
    node['type'] = NodeType.TX_ANCHOR;
    node['label'] = null;
    node = domainRenderer.renderTxAnchor(node);
    return node;
}

export const fillAttributeFigure = (node: object | any, indexId: number) => {
    node['type'] = NodeType.ATTRIBUTE;
    node = domainRenderer.renderAttribute(node);
    if (node['extendedColumn'] != null && node["extendedColumn"].length != 0) {
        if (node["timeRange"] != null) {
            node = domainRenderer.renderHistoricalCortegeAttribute(node);
        } else {
            node = domainRenderer.renderCortegeAttribute(node);
        }
    } else {
        if (node["timeRange"] != null) {
            node = domainRenderer.renderHistoricalAttribute(node);
        } else {
            node = domainRenderer.renderAttribute(node);
        }
    }
    return node;
}

export const addId = (column: any, indexId: number) => {
    column['id'] = (indexId).toString();
    return column;
}

export const fillKnotFigure = (node: object | any) => {
    node = domainRenderer.renderKnot(node);
    node['type'] = NodeType.KNOT;
    return node;
}

export const fillTieFigure = (node: object | any) => {
    node['type'] = NodeType.TIE;
    if (node["timeRange"] != null) {
        // this.historicalTies.push(node["id"]);
        node = domainRenderer.renderHistoricalTie(node);
    } else {
        node = domainRenderer.renderTie(node);
    }
    return node;
}

export const createAttributeNodeTemplate = (network: Network, activeNode: Node) => {
    let mnemonic = randomBytes(2).toString('hex').substring(0, 3);
    let node = {
        "id": network.getSelectedNodes()[0].toString() + "_" + mnemonic,
        "label": "AttributeExample",
        "descriptor": "AttributeExample",
        "description": "Attribute Example Description",
        "mnemonic": mnemonic,
        "type": NodeType.ATTRIBUTE,
        "x": activeNode!.x,
        "y": activeNode!.y,
        "fixed": false,
        "knotRange": null,
        // "oldMnemonic": mnemonic,
        "uid": uuid()
    }
    return node;
}

export const createTieNodeTemplate = (lastId: number, activeNode: Node) => {
    let mnemonic = lastId.toString();
    let node = {
        "id": mnemonic,
        "label": null,
        "descriptor": null,
        "description": "Tie Example Description",
        "type": NodeType.TIE,
        "x": activeNode!.x,
        "y": activeNode!.y,
        "fixed": false,
        "uid": uuid(),
        "extendedColumn": [],
        "isNew": true
    }
    return node;
}

export const createTxAnchorNodeTemplate = (lastId: number, activeNode: Node) => {
    let mnemonic = randomBytes(2).toString('hex').substring(0, 2);
    let node = {
        "id": mnemonic,
        "label": null,
        "descriptor": null,
        "description": "Tie Example Description",
        "type": NodeType.TX_ANCHOR,
        "x": activeNode!.x,
        "y": activeNode!.y,
        "fixed": false,
        "uid": uuid(),
        "extendedColumn": [],
        "isNew": true
    }
    return node;
}

export const createAnchorNodeTemplate = (activeNode: Node) => {
    let mnemonic = randomBytes(2).toString('hex').substring(0, 2);
    let node = {
        "id": mnemonic,
        "label": "AnchorExample",
        "descriptor": "AnchorExample",
        "description": "Anchor Example Description",
        "mnemonic": mnemonic,
        "type": NodeType.ANCHOR,
        "x": activeNode != null ? activeNode!.x : 700,
        "y": activeNode != null ? activeNode!.y : 700,
        "fixed": false,
        "attribute": [],
        // "oldMnemonic": mnemonic,
        "isNew": true,
        "uid": uuid(),
        "extendedColumn": []
    }
    return node
}

export const createKnotNodeTemplate = (activeNode: Node) => {
    let mnemonic = randomBytes(2).toString('hex').substring(0, 3);
    let node = {
        "id": mnemonic,
        "label": "KnotExample",
        "descriptor": "KnotExample",
        "description": "Knot Example Description",
        "mnemonic": mnemonic,
        "type": NodeType.KNOT,
        "x": 0,
        "y": 0,
        "fixed": false,
        "knotRole": null,
        "values": [{item: []}],

        "metadata": {
            "privacy": null,
            "capsule": null,
            "restatable": false,
            "generator": false,
            "deletable": false,
            "idempotent": false
        },
        "dataRange": "BIGSTRING",
        "identity": "BIGINT",
        "skip": false,
        "deprecated": false,

        "isNew": true,
        "uid": uuid()
    }
    return node
}

