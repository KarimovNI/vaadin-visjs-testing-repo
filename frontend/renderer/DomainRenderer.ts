import {Node} from "vis-network";


export class DomainRenderer {

    private anchorBorderColor = "#f66";
    private anchorBackgroundColor = "#f66";

    private tieBorderColor = "#5b5b5b";
    private tieBackgroundColor = "#f4f4f4";

    private knotBorderColor = "#f66";
    private knotBackgroundColor = "#ffffff";

    private attributeBorderColor = "#f66";
    private attributeBackgroundColor = "#ffffff";

    private selectedBorderColor = "#2B7CE9";
    private selectedBackgroundColor = "#D2E5FF";

    private nodeSize = 20;
    private nodeRadius = 18;

    private fontColor = "#000000";

    private angleRadius = 5;

    renderAnchor(node: Node){
        let _this = this;
        node["shape"] = "custom";
        // @ts-ignore
        node['ctxRenderer'] = ({ ctx, id, x, y, state: { selected, hover }, style, label }) => {
            return {
                drawNode() {
                    ctx.beginPath();
                    ctx.lineWidth = 2;

                    if(!selected){
                        ctx.fillStyle = _this.anchorBackgroundColor;
                        ctx.strokeStyle = _this.anchorBorderColor
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor;
                    }
                    ctx.rect(x - _this.nodeSize, y - _this.nodeSize, _this.nodeSize * 2, _this.nodeSize * 2);
                    ctx.fill();
                    ctx.stroke();
                },
                drawExternalLabel() {
                    ctx.textAlign = 'center';
                    ctx.fillStyle = _this.fontColor;
                    ctx.fillText(label == null ? "" : label, x, y + 35);
                }
            };
        }
        return node;
    }
    renderTxAnchor(node: Node){
        let _this = this;
        node["shape"] = "custom";
        // @ts-ignore
        node['ctxRenderer'] = ({ ctx, id, x, y, state: { selected, hover }, style, label }) => {
            return {
                drawNode() {
                    ctx.beginPath();
                    ctx.lineWidth = 2;
                    if(!selected){
                        ctx.fillStyle = _this.anchorBackgroundColor;
                        ctx.strokeStyle = _this.anchorBorderColor
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor;
                    }
                    ctx.rect(x - _this.nodeSize, y - _this.nodeSize, _this.nodeSize * 2, _this.nodeSize * 2);
                    ctx.fill();
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.lineWidth = 2;
                    if(!selected){
                        ctx.fillStyle = "#ffffff";
                        ctx.strokeStyle = _this.anchorBackgroundColor;
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor;
                    }
                    ctx.rect(x - 6, y - 25, 25, 15);
                    ctx.fill();
                    ctx.stroke();

                    ctx.fillStyle = _this.fontColor;
                    ctx.font = "bold 12px arial";
                    ctx.fillText(
                        "TX",
                        x - 1,
                        y - 13,
                    );

                },
                drawExternalLabel() {
                    ctx.textAlign = 'center';
                    ctx.fillStyle = _this.fontColor;
                    ctx.fillText(label == null ? "" : label, x, y + 35);
                }
            };
        }
        return node;
    }

    renderTie(node: Node){
        node["color"] = {
            border: this.tieBorderColor,
            background: this.tieBackgroundColor
        };
        node["shape"] = "diamond";
        node['size'] = this.nodeSize;
        return node;
    }
    renderHistoricalTie(node: Node){
        let _this = this;
        node["shape"] = "custom";
        // @ts-ignore
        node['ctxRenderer'] = ({ ctx, id, x, y, state: { selected, hover }, style, label }) => {
            return {
                drawNode() {
                    ctx.beginPath();
                    ctx.lineWidth = 1;
                    if (!selected) {
                        ctx.fillStyle = _this.tieBackgroundColor;
                        ctx.strokeStyle = _this.tieBorderColor;
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor
                    }

                    ctx.save();
                    ctx.beginPath();
                    ctx.lineWidth = 1;
                    ctx.moveTo(x, y - _this.nodeSize);
                    ctx.lineTo(x - _this.nodeSize, y );
                    ctx.lineTo(x, y + _this.nodeSize);
                    ctx.lineTo(x + _this.nodeSize, y);
                    ctx.closePath();
                    ctx.fill();
                    ctx.restore();
                    ctx.stroke();

                    if(selected){
                        ctx.strokeStyle = _this.selectedBorderColor
                    } else {
                        ctx.strokeStyle = _this.tieBorderColor;
                    }
                    ctx.lineWidth = 1;
                    ctx.beginPath();
                    ctx.moveTo(x, y - _this.nodeSize + 10);
                    ctx.lineTo(x - _this.nodeSize + 10, y );
                    ctx.lineTo(x, y + _this.nodeSize - 10);
                    ctx.lineTo(x + _this.nodeSize - 10, y);

                    ctx.closePath();

                    ctx.stroke();
                },
                drawExternalLabel() {
                    ctx.textAlign = 'center';
                    ctx.fillStyle = _this.fontColor;
                    ctx.fillText(label, x, y + 35);
                }
            };
        }
        return node;
    }

    renderKnot(node: Node){
        let _this = this;
        node["shape"] = "custom";
        // @ts-ignore
        node['ctxRenderer'] = ({ ctx, id, x, y, state: { selected, hover }, style, label }) => {
            return {
                drawNode() {
                    ctx.beginPath();
                    ctx.lineWidth = 2;
                    if(!selected){
                        ctx.fillStyle = _this.knotBackgroundColor;
                        ctx.strokeStyle = _this.knotBorderColor
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor;
                    }
                    ctx.rect(x - _this.nodeSize, y - _this.nodeSize, _this.nodeSize * 2, _this.nodeSize * 2);
                    ctx.fill();
                    ctx.stroke();
                },
                drawExternalLabel() {
                    ctx.textAlign = 'center';
                    ctx.fillStyle = _this.fontColor;
                    ctx.fillText(label == null ? "" : label, x, y + 35);
                }
            };
        }

        return node;
    }

    renderAttribute(node: Node){
        node['shape'] = 'dot';
        node["color"] = {
            border: this.attributeBorderColor,
            background: this.attributeBackgroundColor
        };
        node['size'] = this.nodeRadius;
        node["borderWidth"] = 2;
        return node;
    }
    public renderHistoricalAttribute(node: Node){
        let _this = this;
        node["shape"] = "custom";
        // @ts-ignore
        node['ctxRenderer'] = ({ ctx, id, x, y, state: { selected, hover }, style, label }) => {
            return {
                drawNode() {
                    ctx.beginPath();
                    ctx.lineWidth = 2;
                    if(!selected){
                        ctx.fillStyle = _this.attributeBackgroundColor;
                        ctx.strokeStyle = _this.attributeBorderColor;
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor
                    }
                    ctx.arc(x, y, _this.nodeRadius, 0, 2 * Math.PI);
                    ctx.fill();
                    ctx.stroke()
                    ctx.beginPath();
                    ctx.lineWidth = 2;

                    ctx.arc(x, y, _this.nodeRadius - 7, 0, 2 * Math.PI);
                    ctx.stroke();
                },
                drawExternalLabel() {
                    ctx.textAlign = 'center';
                    ctx.fillStyle = _this.fontColor;
                    ctx.fillText(label, x, y + 35);
                }
            };
        }
        return node;
    }
    renderCortegeAttribute(node: Node){
        let _this = this;
        node["shape"] = "custom";
        // @ts-ignore
        node['ctxRenderer'] = ({ ctx, id, x, y, state: { selected, hover }, style, label }) => {
            return {
                drawNode() {
                    ctx.beginPath();
                    ctx.lineWidth = 2;

                    if(!selected){
                        ctx.fillStyle = _this.attributeBackgroundColor;
                        ctx.strokeStyle = _this.attributeBorderColor;
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor
                    }

                    ctx.arc(x, y, _this.nodeRadius, 0, 2 * Math.PI);
                    ctx.fill();
                    ctx.stroke()

                    ctx.beginPath();
                    ctx.lineWidth = 2;
                    ctx.arc(x + _this.nodeRadius + 10, y, _this.nodeRadius, 0, 2 * Math.PI);
                    ctx.fill();
                    ctx.stroke()


                },
                drawExternalLabel() {
                    ctx.textAlign = 'center';
                    ctx.fillStyle = _this.fontColor;
                    ctx.fillText(label, x, y + 35);
                }
            };
        }
        return node;
    }
    renderHistoricalCortegeAttribute(node: Node){
        let _this = this;
        node["shape"] = "custom";
        // @ts-ignore
        node['ctxRenderer'] = ({ ctx, id, x, y, state: { selected, hover }, style, label }) => {
            return {
                drawNode() {
                    ctx.beginPath();
                    ctx.lineWidth = 2;

                    if(!selected){
                        ctx.fillStyle = _this.attributeBackgroundColor;
                        ctx.strokeStyle = _this.attributeBorderColor;
                    } else {
                        ctx.fillStyle = _this.selectedBackgroundColor;
                        ctx.strokeStyle = _this.selectedBorderColor
                    }
                    ctx.arc(x, y, _this.nodeRadius, 0, 2 * Math.PI);
                    ctx.fill();
                    ctx.stroke()
                    ctx.beginPath();
                    ctx.lineWidth = 2;

                    ctx.arc(x, y, _this.nodeRadius - 7, 0, 2 * Math.PI);
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.lineWidth = 2;

                    ctx.arc(x + _this.nodeRadius + 10, y, _this.nodeRadius, 0, 2 * Math.PI);
                    ctx.fill()
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.lineWidth = 2;

                    ctx.arc(x + _this.nodeRadius + 10, y, _this.nodeRadius - 7, 0, 2 * Math.PI);
                    ctx.fill()
                    ctx.stroke();
                },
                drawExternalLabel() {
                    ctx.textAlign = 'center';
                    ctx.fillStyle = _this.fontColor;
                    ctx.fillText(label, x, y + 35);
                }
            };
        }
        return node;
    }
}