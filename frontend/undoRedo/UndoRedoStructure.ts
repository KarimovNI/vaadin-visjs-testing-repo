import {Edge, Node} from "vis-network";
import {FullItem} from "vis-data/declarations/data-interface"

export const copy = (obj: any)=> {
    if (obj === null) return null;
    let clone = Object.assign({}, obj);
    Object.keys(clone).forEach(
        key =>
            (clone[key] =
                typeof obj[key] === 'object' ? copy(obj[key]) : obj[key])
    );
    if (Array.isArray(obj)) {
        clone.length = obj.length;
        return Array.from(clone);
    }
    return clone;
};

export let history : {
    past: {nodes: FullItem<Node, "id">[], edges: FullItem<Edge, "id">[]}[],
    current: {nodes: FullItem<Node, "id">[], edges: FullItem<Edge, "id">[]} | null,
    future: {nodes: FullItem<Node, "id">[], edges: FullItem<Edge, "id">[]}[],
} = {
    past: [],
    current: null,
    future: []
}

export const redoHistory = () => {
    history.past.push(copy(history.current));
    history.current = copy(history.future[0]);
    history.future.shift();
    return history;
}

export const undoHistory = () => {
    history.future.splice(0, 0, copy(history.current!));
    history.current = copy(history.past[history.past.length - 1]);
    history.past.pop();
    return history;
}

export const initHistory = (edges: FullItem<Edge, "id">[], nodes: FullItem<Node, "id">[]) => {
    history.current =copy({
        nodes: nodes,
        edges: edges
    });

    return history;
}

export const updateHistory = (edges: FullItem<Edge, "id">[], nodes: FullItem<Node, "id">[]) => {
    history.past.push(copy(history.current));
    history.current = copy({
        nodes: nodes,
        edges: edges
    });
    history.future = [];
    return history;
}

export const getDifferenceInNodes = (A: FullItem<Node, "id">[], B: FullItem<Node, "id">[]) => {
    return getDifference(A, B);
}

export const getDifferenceInNodesByKeys = (A: FullItem<Node, "id">[], B: FullItem<Node, "id">[]) => {
    return getDifferenceByKeys(A, B);
}

export const getDifferenceInEdges = (A: FullItem<Edge, "id">[], B: FullItem<Edge, "id">[]) => {
    return getDifference(A, B);
}

const getDifference = (A: FullItem<Edge | Node, "id">[], B: FullItem<Edge | Node, "id">[]) => {
    const idOfB = B.map(x => {return x.id});
    return  A.filter(x => !idOfB.includes(x.id));
}

export const getDifferenceInEdgesByKeys = (A: FullItem<Edge, "id">[], B: FullItem<Edge, "id">[]) => {
    return getDifferenceByKeys(A, B);
}

const getDifferenceByKeys = (A: FullItem<Edge | Node, "id">[], B: FullItem<Edge | Node, "id">[]) => {
    const idOfA = A.map(x => {return x.id});
    const idOfB = B.map(x => {return x.id})
    let difference = [];
    for (let id of idOfA){
        const objFromA = copy(A.find(x => x.id == id));
        delete objFromA['x'];
        delete objFromA['y'];
        delete objFromA['fixed']
        const objFromB = copy(B.find(x => x.id == id));
        delete objFromB['x'];
        delete objFromB['y'];
        delete objFromB['fixed']
        if (JSON.stringify(objFromA) != JSON.stringify(objFromB)){
            difference.push(objFromA as Node);
        }
    }
    return difference;
}