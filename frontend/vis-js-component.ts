import {css, html, LitElement, PropertyValues} from 'lit';
import {customElement, property, state} from 'lit/decorators';
import '@vaadin/menu-bar';
import {createTemplate} from "Frontend/icons/get-icons";
import {Edge, IdType, Network, Node} from "vis-network";
import {DataSet} from "vis-data";
import '@vaadin/vertical-layout';

import "Frontend/custom-lit-components/menu-bar-components/LeftSideMenuBar";
import "Frontend/custom-lit-components/menu-bar-components/RightSideMenuBar";
import {AnchorEditorLayout} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/AnchorEditorLayout";
import {AttributeEditorLayout} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/AttributeEditorLayout";
import {KnotEditorLayout} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/KnotEditorLayout";
import {DomainRenderer} from "Frontend/renderer/DomainRenderer";
import {TieEditorLayout} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/TieEditorLayout";
import {TxAnchorLayout} from "Frontend/custom-lit-components/network-editor-components/node-editor-components/TxAnchorLayout";

import {NodeType} from "Frontend/enums/NodeType";
import {EdgeEditorDialog} from "Frontend/custom-lit-components/network-editor-components/edge-editor-components/dialog/EdgeEditorDialog";
import {Id} from "vis-network/declarations/network/modules/components/edges";


import {
    copy, getDifferenceInEdges, getDifferenceInEdgesByKeys,
    getDifferenceInNodes, getDifferenceInNodesByKeys, initHistory,
    redoHistory, undoHistory, updateHistory
} from "Frontend/undoRedo/UndoRedoStructure";
import {FullItem} from "vis-data/declarations/data-interface";

import {mapNodeToAnchor, mapNodeToKnot, mapNodeToTie, mapNodeToTxAnchor} from "Frontend/interfaces/Mappers";
import {createAnchorNodeTemplate, createAttributeNodeTemplate, createKnotNodeTemplate,
    createTieNodeTemplate, createTxAnchorNodeTemplate, fillAnchorFigure, fillAttributeFigure,
    fillKnotFigure, fillTieFigure, fillTxAnchorFigure,
    parseNodeLayout, parseNodeMainData} from "Frontend/node-parsing/Parsing";
import {DataRange} from "Frontend/enums/DataRange";
import {SheetType} from "Frontend/enums/SheetType";
import {randomBytes} from "crypto";


const template = createTemplate();
document.head.appendChild(template.content);


async function getNewFileHandle(s: string) {
    const opts = {
        types: [{
            description: 'Text file',
            accept: {'application/xml': ['.xml']},
        }],
    };
    const handle = await window.showSaveFilePicker(opts);
    const writable = await handle.createWritable()
    await writable.write(s)
    await writable.close()
}

@customElement('custom-tag')
export class VisJsComponent extends LitElement {

    static get styles() {
        return css`
      .text-field-width{
        width: 100%;
      }
      anchor-editor-layout{
        width: 100%;
      }
      tie-editor-layout{
        width: 100%;
      }
      knot-editor-layout{
        width: 100%;
      }
      attribute-editor-layout{
        width: 100%;
      }
      tx-anchor-editor-layout{
        width: 100%;
      }
      [slot="label"] {
        font-size: medium;
        font-weight: normal ;
      }
      [theme="spacing-padding"] {
        height: 350px;
        width: 100%;
        padding-top: 0;
      }
      [theme="spacing-xs padding"]{
        padding-top: 0;
                height: 350px;

      }
       #customId {
        padding: 0;
        width: 99vw;
        margin: 0;
        
      }
      #bottomPanel {
        z-index: 2; 
        left: 20px;
        bottom: 15px;
        width: 98%;
        background-color: white; 
        border-style: solid; 
        border-color:  #006AF5; 
        border-radius: 3px;
        border-width: 1px;
      }
      #customId::-webkit-scrollbar {
        width: 15px;
        background-color: #f4f4f4;
        border-radius: 5px;
      }
      #customId::-webkit-scrollbar-thumb {
        width: 15px;
        background-color: #dbdbdb;
        border-radius: 5px;
      }
  }
`;
    }

    @state()
    private edgeDialog: EdgeEditorDialog = new EdgeEditorDialog();

    private domainRenderer: DomainRenderer = new DomainRenderer();

    @property()
    private edgeList: Edge[] = [];

    @state()
    private knots!: string[];

    @state()
    private nodeDataSet = new DataSet<Node>([]);
    @state()
    private edgeDataSet = new DataSet<Edge>([]);

    @property()
    private lastId = 1000;

    @property()
    private indexId = 1000;

    @property()
    private edgeId = 1000;

    private scale = 1;

    @property()
    private layoutHeight = '97';
    @property()
    private layoutWidth = '120';
    @property()
    private activeNode: Node | null = null;
    @property()
    private activeEdge: Edge | null = null;

    private activeFieldName: string | null = null;

    @property()
    private pinAll = false;

    @state()
    private buffered : boolean = false;

    @state()
    private fixedNodes: string[] = [];

    @property()
    private network!: Network;

    private history!: {
        past: {nodes: FullItem<Node, "id">[], edges: FullItem<Edge, "id">[]}[],
        current: {nodes: FullItem<Node, "id">[], edges: FullItem<Edge, "id">[]} | null,
        future: {nodes: FullItem<Node, "id">[], edges: FullItem<Edge, "id">[]}[],
    };

    private $server?: VisJsComponentServerInterface;

    @property()
    private metadataLayoutVisibility = 'none';


    @property()
    private anchorsToDelete: string[] = []
    @property()
    private tiesToDelete: string[] = []
    @property()
    private knotsToDelete: string[] = []
    @property()
    private attributesToDelete: string[] = []
    @property()
    private txAnchorsToDelete: string[] = []



    private nodeAddingEventHandlerStructure: { [element: string]: () => void } = {
        'anchor-add': () => {
            this.buffered = true;
            this.fillAnchorKnotRoleByAnchor(this.addAnchor());
            this.closeMetadataLayout();
        },
        'connect': () => {
            this.buffered = true;
            this.connectKnotWithTieOrAttribute();
            this.closeMetadataLayout();
        },
        'connect-anchor': () => {
            this.buffered = true;
            this.connectAnchorToTie();
            this.closeMetadataLayout();
        },
        'knot-add': () => {
            this.buffered = true;
            //
            this.closeMetadataLayout();
        },
        'anchor-tx-add': () => {
            this.buffered = true;
            this.addTxAnchor();
            this.closeMetadataLayout();
        },
        'tie-add': () => {
            this.buffered = true;
            this.addTie();
            this.closeMetadataLayout();
        },
        'tie-self-add': () => {
            this.buffered = true;
            this.addSelfTie();
            this.closeMetadataLayout();
        },
        'tie-his-add': () => {
            this.buffered = true;
            this.addHistoricalTie();
            this.closeMetadataLayout();
        },
        'tie-a-add': () => {
            this.buffered = true;
            this.addAnchoredTie();
            this.closeMetadataLayout();
        },
        'tie-his-a-add': () => {
            this.buffered = true;
            this.addAnchoredHistoricalTie();
            this.closeMetadataLayout();
        },
        'attribute-add': () => {
            this.buffered = true;
            this.addAttribute();
            this.closeMetadataLayout();
        },
        'attribute-his-add': () => {
            this.buffered = true;
            this.addHistoricalAttribute();
            this.closeMetadataLayout();
        },
        'attribute-composed-add': () => {
            this.buffered = true;
            this.addComposedAttribute();
            this.closeMetadataLayout();
        }
    }

    @property()
    private chosenNodeType = NodeType.NO_TYPE;

    @property()
    private renderingComponent: AnchorEditorLayout | KnotEditorLayout | AttributeEditorLayout | TieEditorLayout | TxAnchorLayout | null = null;

    render() {
        this.edgeDialog.addEventListener('deselect-edge', () => this.network.unselectAll());
        this.edgeDialog.addEventListener('edit-edge', (e: Event) => {
            e.stopImmediatePropagation(); //@ts-ignore
            if(this.activeNode.indexes != null && this.activeNode.indexes.length != 0){ //@ts-ignore
                let indexes = this.activeNode.indexes[0].index;
                for(let index of indexes){
                    for(let columnIndex = 0; columnIndex < index.columns.column.length; columnIndex++){
                        if(index.columns.column[columnIndex].includes(this.activeEdge?.label?.split(",")[0])){
                            const oldRole = index.columns.column[columnIndex]; //@ts-ignore
                            index.columns.column[columnIndex] = oldRole.split("_")[0] + "_" + e.detail.role + " " + oldRole.split(" ")[1];
                        }
                    }
                }
            }

            // @ts-ignore
            if(e.detail.buffered){
                if(this.activeEdge?.to!.toString().length == 3){ // is it knot checking
                    // @ts-ignore
                    this.activeNode!.knotRole[0].identifier = e.detail.identifier;
                    // @ts-ignore
                    this.activeNode!.knotRole[0].role = e.detail.role;
                    // @ts-ignore
                    this.activeNode!.knotRole[0].description = e.detail.description;
                } else { //@ts-ignore
                    const index = this.activeNode!.anchorRole.findIndex((x: any) => x.id == this.activeEdge?.id);
                    // @ts-ignore
                    this.activeNode!.anchorRole[index].identifier = e.detail.identifier;
                    // @ts-ignore
                    this.activeNode!.anchorRole[index].role = e.detail.role;
                    // @ts-ignore
                    this.activeNode!.anchorRole[index].description = e.detail.description;
                }
                // @ts-ignore
                this.activeEdge!.label = e.detail.role + (e.detail.identifier == true? ", |||" : ", |")
                this.nodeDataSet.update(this.activeNode!);
                this.edgeDataSet.update(this.activeEdge!);
                this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            }

        });
        return html `

            <div style="position: fixed; z-index: 1; background-color: transparent; left: 20px;">
                <left-side-menu-bar
                        @add-node-event="${(e: CustomEvent) => {this.nodeAddingEventHandlerStructure[e.detail]()}}"
                        .chosenNodeType="${this.chosenNodeType}"
                        style=" background-color: hsla(0, 0%, 100%, 0.3); "
                >
                </left-side-menu-bar>
            </div>
            <div style="position: fixed; z-index: 1; background-color: transparent; right: 20px">
                <right-side-menu-bar
                        style=" background-color: hsla(0, 0%, 100%, 0.3);"
                        @zoom-event="${(e: CustomEvent) => this.handleZoom(e)}"
                        @pin-event="${(e: CustomEvent) => this.handlePin(e)}"
                        @search-event="${(e: CustomEvent) => this.handleSearch(e)}"
                        @download-event="${(e: CustomEvent) => {this.handleDownload(e);}}"
                        @undo-event="${(e: CustomEvent) => this.handleUndoEvent(e)}"
                        @redo-event="${(e: CustomEvent) => this.handleRedoEvent(e)}"
                >
                </right-side-menu-bar>
            </div>
            <div
                    id="customId"
                    style="height: ${this.layoutHeight}vh"
            >
            </div>
            <div
                    id="bottomPanel" style="display: ${this.metadataLayoutVisibility}; position: fixed">
                ${this.renderingComponent}
            </div>
            ${this.edgeDialog}
        `
    }

    saveToFile(xmlContent: string){
        getNewFileHandle(xmlContent).then(r => console.log(r)).catch(r => console.log(r))
    }


    // неудобоваримое безобразие достойное переработки — два метода ниже
    removeKnotRangeRoleFromNodes(knot: Node) {
        let attributesAndKnots = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return item.type == NodeType.ATTRIBUTE || item.type == NodeType.TIE;
            }
        });
        let attributes = attributesAndKnots.filter(node =>
            // @ts-ignore
            (node.type == NodeType.ATTRIBUTE && node.knotRange == knot.mnemonic)
        ).map(node => { // @ts-ignore
            node.knotRange = null; return node});

        // @ts-ignore
        let knotes = attributesAndKnots.filter(node =>(node.type == NodeType.TIE && node.knotRole != null && node.knotRole.length != 0 && node.knotRole[0].type == knot.mnemonic)).map(
            node => { // @ts-ignore
                node.knotRole = null; return node}
        );

        this.nodeDataSet.update(attributes);
        this.nodeDataSet.update(knotes);
    }
    addKnotRoleRange(knots: Node[]){
        let nodesToUpdate: Node[] = []
        for(let knot of knots) {
            for(let edge of this.edgeDataSet.get()){
                let id = null;
                if(edge.from == knot.id){
                    id = edge.to;
                }
                if(edge.to == knot.id){
                    id = edge.from;
                }
                if (id != null){
                    let node = this.nodeDataSet.get(id as Id);
                    console.log(edge)
                    // @ts-ignore
                    if(node.type == NodeType.ATTRIBUTE){
                        // @ts-ignore
                        node.knotRange = knot.mnemonic;
                    } else {
                        // @ts-ignore
                        node.knotRole = [{description: edge.description, role: edge.label, type: knot.mnemonic, identifier: edge.identifier}]
                    }
                    nodesToUpdate.push(node as Node);
                }
            }
        }
        this.nodeDataSet.update(nodesToUpdate);
    }

    handleRedoEvent(e: CustomEvent<string>){
        if(this.history.future.length > 0){
            let usedSheet = null;
            if(this.renderingComponent != null){
                usedSheet = this.renderingComponent?.getSelectedSheet();
            }
            const currentNodes = this.nodeDataSet.get();
            const currentEdges = this.edgeDataSet.get();
            const futureNodes = this.history.future[0].nodes;
            const futureEdges = this.history.future[0].edges;

            let nodeDifferences;
            let knots: FullItem<Node | Edge, "id">[] = [];
            let attributes: FullItem<Node | Edge, "id">[] = [];
            let anchors: FullItem<Node | Edge, "id">[] = [];

            if (currentNodes.length > futureNodes.length) {
                nodeDifferences = getDifferenceInNodes(currentNodes, futureNodes);
                nodeDifferences.map(node => this.deleteSwitch(node));
                this.nodeDataSet.remove(nodeDifferences as Node[]);
                const ids = nodeDifferences.map(node => {return node.id});
                this.fixedNodes = this.fixedNodes.filter((node: any) => ids.indexOf(node.id) < 0)
                // @ts-ignore
                nodeDifferences = nodeDifferences.filter(node => node.type == NodeType.KNOT);
                for(let node of nodeDifferences){
                    this.removeKnotRangeRoleFromNodes(node as Node);
                }
                for(let node of nodeDifferences.filter((node: any) => node.type == NodeType.ATTRIBUTE)){
                    this.removeAttributeFromAnchor(node as Node);
                }
                for(let node of nodeDifferences.filter((node: any) => node.type == NodeType.ANCHOR)){
                    this.removeAnchorRoleFromTieAndTxAnchor(node as Node);
                }
            } else {
                if(currentNodes.length < futureNodes.length) {
                    nodeDifferences = getDifferenceInNodes(futureNodes, currentNodes);
                    nodeDifferences.map(node => this.reincarnationSwitch(node));
                    // @ts-ignore
                    knots = nodeDifferences.filter(node => node.type == NodeType.KNOT); // @ts-ignore
                    attributes = nodeDifferences.filter(node => node.type == NodeType.ATTRIBUTE); // @ts-ignore
                    anchors = nodeDifferences.filter(node => node.type == NodeType.ANCHOR);
                } else {
                    nodeDifferences = getDifferenceInNodesByKeys(futureNodes, currentNodes);
                }
                nodeDifferences = nodeDifferences.map((node: Node | Edge) => this.fillFigures(node as Node));
                this.nodeDataSet.update(nodeDifferences);
            }

            let edgeDifferences;

            if (currentEdges.length > futureEdges.length) {
                edgeDifferences = getDifferenceInEdges(currentEdges, futureEdges);
                this.edgeDataSet.remove(edgeDifferences);
            } else {
                if(currentEdges.length < futureEdges.length){
                    edgeDifferences = getDifferenceInEdges(futureEdges, currentEdges);
                } else {
                    edgeDifferences = getDifferenceInEdgesByKeys(futureEdges, currentEdges);
                }
                this.edgeDataSet.update(edgeDifferences);
            }
            this.addKnotRoleRange(knots as Node[]);
            this.addAttributeToAnchor(attributes as Node[]);
            this.addAnchorRoleToTieOrTxAnchor(anchors as Node[]);

            this.history = redoHistory();
            if(this.renderingComponent != null){
                console.log("!$!")
                this.activeNode = this.getSelectedNode();
                if(this.activeNode != null){
                    this.updateMetadataLayout();
                }
                if(usedSheet != null){
                    this.renderingComponent.setSelectedSheet(usedSheet as number);
                }
            }
        }
    }

    fillFigures(node: Node){
        // @ts-ignore
        switch (node.type) {
            case (NodeType.ANCHOR): {
                node = fillAnchorFigure(node);
                break;
            }
            case (NodeType.TIE): {
                node = fillTieFigure(node);
                break;
            }
            case (NodeType.ATTRIBUTE): {
                // console.log(copy(node))
                // node = this.domainRenderer.renderAttribute(node);
                // node = this.setPositions(node);
                // this.nodeDataSet.update(node);
                node = fillAttributeFigure(node, ++this.indexId);
                break;
            }
            default : {
                break;
            }
        }
        return node;
    }

    // Поразмыслить тут
    deleteSwitch(node: any){
        switch (node.type) {
            case NodeType.ANCHOR :{
                this.anchorsToDelete.push(node['uid'])
                break;
            }
            case NodeType.TIE :{
                this.tiesToDelete.push(node['uid'])
                break;
            }
            case NodeType.KNOT :{
                this.knotsToDelete.push(node['uid'])
                break;
            }
            case NodeType.ATTRIBUTE :{
                this.attributesToDelete.push(node['uid'])
                break;
            }
            case NodeType.TX_ANCHOR :{
                this.txAnchorsToDelete.push(node['uid'])
                break;
            }
        }
        console.log(this.anchorsToDelete)
        console.log(this.tiesToDelete)
        console.log(this.knotsToDelete)
        console.log(this.attributesToDelete)
        console.log(this.txAnchorsToDelete)


    }
    reincarnationSwitch(node: any){
        switch (node.type) {
            case NodeType.ANCHOR :{
                this.anchorsToDelete.splice(this.anchorsToDelete.indexOf(node['uid']), 1);
                break;
            }
            case NodeType.TIE :{
                this.tiesToDelete.splice(this.tiesToDelete.indexOf(node['uid']), 1);
                break;
            }
            case NodeType.KNOT :{
                this.knotsToDelete.splice(this.knotsToDelete.indexOf(node['uid']), 1);
                break;
            }
            case NodeType.ATTRIBUTE :{
                this.attributesToDelete.splice(this.attributesToDelete.indexOf(node['uid']), 1);
                break;
            }
            case NodeType.TX_ANCHOR :{
                this.txAnchorsToDelete.splice(this.txAnchorsToDelete.indexOf(node['uid']), 1);
                break;
            }
        }
        console.log(this.anchorsToDelete)
        console.log(this.tiesToDelete)
        console.log(this.knotsToDelete)
        console.log(this.attributesToDelete)
        console.log(this.txAnchorsToDelete)
    }

    handleUndoEvent(e: CustomEvent<string>){

        if(this.history.past.length >= 1){
            let usedSheet = null;
            if(this.renderingComponent != null){
                usedSheet = this.renderingComponent?.getSelectedSheet();
            }
            const currentNodes = this.nodeDataSet.get();
            const currentEdges = this.edgeDataSet.get();
            const pastNodes = this.history.past[this.history.past.length - 1].nodes;
            const pastEdges = this.history.past[this.history.past.length - 1].edges;

            let nodeDifferences;
            let knots: Node[] = [];
            let attributes: Node[] = [];
            let anchors: Node[] = [];

            if (currentNodes.length > pastNodes.length) {
                nodeDifferences = getDifferenceInNodes(currentNodes, pastNodes);
                nodeDifferences.map(node => this.deleteSwitch(node));
                this.nodeDataSet.remove(copy(nodeDifferences) as Node[]);
                // @ts-ignore
                const ids = nodeDifferences.map(node => {return node.id});
                this.fixedNodes = this.fixedNodes.filter((node: any) => ids.indexOf(node.id) < 0)
                for(let node of nodeDifferences.filter((node: any) => node.type == NodeType.KNOT)){
                    this.removeKnotRangeRoleFromNodes(node as Node);
                }
                for(let node of nodeDifferences.filter((node: any) => node.type == NodeType.ATTRIBUTE)){
                    this.removeAttributeFromAnchor(node as Node);
                }
                for(let node of nodeDifferences.filter((node: any) => node.type == NodeType.ANCHOR)){
                    this.removeAnchorRoleFromTieAndTxAnchor(node as Node);
                }
            } else {
                if(currentNodes.length < pastNodes.length){
                    nodeDifferences = getDifferenceInNodes(pastNodes, currentNodes);
                    nodeDifferences.map(node => this.reincarnationSwitch(node));
                    console.log(copy(nodeDifferences))
// @ts-ignore
                    knots = nodeDifferences.filter(node => node.type == NodeType.KNOT);                    // @ts-ignore
                    attributes = nodeDifferences.filter(node => node.type == NodeType.ATTRIBUTE);// @ts-ignore
                    anchors = nodeDifferences.filter(node => node.type == NodeType.ANCHOR);
                } else {
                    nodeDifferences = getDifferenceInNodesByKeys(pastNodes, currentNodes);
                    console.log(copy(nodeDifferences))

                }
                nodeDifferences = nodeDifferences.map((node: Node | Edge) => this.fillFigures(node as Node));
                this.nodeDataSet.update(nodeDifferences);
            }

            let edgeDifferences;

            if (currentEdges.length > pastEdges.length) {
                edgeDifferences = getDifferenceInEdges(currentEdges, pastEdges);
                this.edgeDataSet.remove(edgeDifferences);
            } else {
                if(currentEdges.length < pastEdges.length){
                    edgeDifferences = getDifferenceInEdges(pastEdges, currentEdges);
                } else {
                    edgeDifferences = getDifferenceInEdgesByKeys(pastEdges, currentEdges);
                }
                this.edgeDataSet.update(edgeDifferences);
            }
            this.addKnotRoleRange(knots as Node[]);
            this.addAttributeToAnchor(attributes as Node[]);
            this.addAnchorRoleToTieOrTxAnchor(anchors as Node[]);

            this.history = undoHistory();
            if(this.renderingComponent != null){
                console.log("%%%%")
                this.activeNode = this.getSelectedNode();
                if(this.activeNode != null){
                    this.updateMetadataLayout();
                }
                if(usedSheet != null){
                    this.renderingComponent.setSelectedSheet(usedSheet as number);
                }
            }

        }
    }

    removeAnchorRoleFromTieAndTxAnchor(anchor: Node){
        let tiesAndTxAnchors = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return item.type == NodeType.TIE || item.type == NodeType.TX_ANCHOR;
            }
        });
        for (let tieOrTxAnchor of tiesAndTxAnchors){ //@ts-ignore
            let indexToDel = tieOrTxAnchor.anchorRole.findIndex((anchorRole: Node) => anchorRole.type == anchor.mnemonic)
            if(indexToDel != -1){ //@ts-ignore
                tieOrTxAnchor.anchorRole.splice(indexToDel, 1);
            }
            tieOrTxAnchor = this.setPositions(tieOrTxAnchor);
        }
        this.nodeDataSet.update(tiesAndTxAnchors);
    }
    addAnchorRoleToTieOrTxAnchor(anchors: Node[]){
        let nodesToUpdate: Node[] = []
        for(let anchor of anchors) {
            for(let edge of this.edgeDataSet.get()){
                let id = null;
                if(edge.from == anchor.id){
                    id = edge.to;
                }
                if(edge.to == anchor.id){
                    id = edge.from;
                }
                if (id != null){
                    let node = this.nodeDataSet.get(id as Id);
                    // @ts-ignore
                    if(node.type == NodeType.TX_ANCHOR) {
                        console.log(edge)

                        // @ts-ignore
                        const roleTemplate = {description: edge.description, role: edge.label?.split(", ")[0], type: anchor.mnemonic, identifier:  edge.label?.split(", ")[1] != '|', id: edge.id}
                        // @ts-ignore
                        if (node.anchorRole != null && node.anchorRole.length != 0) { //@ts-ignore
                            node.anchorRole.push(roleTemplate);
                        } else { //@ts-ignore
                            node.anchorRole = [roleTemplate];
                        }
                        node = this.setPositions(node);
                        nodesToUpdate.push(node as Node);
                    }
                }
            }
        }
        this.nodeDataSet.update(nodesToUpdate);
    }
    removeAttributeFromAnchor(attribute: Node){
        let anchors = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return item.type == NodeType.ANCHOR || item.type == NodeType.TX_ANCHOR;
            }
        });
        for (let anchor of anchors){ //@ts-ignore
            let indexToDel = anchor.attribute.findIndex((obj: Node) => obj.id == attribute.id);
            if(indexToDel != -1){//@ts-ignore
                anchor.attribute.splice(indexToDel, 1);
            }
            anchor = this.setPositions(anchor);
        }
        this.nodeDataSet.update(anchors);
    }
    addAttributeToAnchor(attributes: Node[]){
        let nodesToUpdate: Node[] = []
        for(let attribute of attributes) {
            for(let edge of this.edgeDataSet.get()){
                let id = null;
                if(edge.from == attribute.id){
                    id = edge.to;
                }
                if(edge.to == attribute.id){
                    id = edge.from;
                }
                if (id != null){
                    let node = this.nodeDataSet.get(id as Id);
                    // @ts-ignore
                    if(node.type == NodeType.ANCHOR || node.type == NodeType.TX_ANCHOR) {
                        // @ts-ignore
                        if (node.attribute != null && node.attribute.length != 0) { //@ts-ignore
                            node.attribute.push(attribute);
                        } else { //@ts-ignore
                            node.attribute = [attribute];
                        }
                        nodesToUpdate.push(node as Node);
                    }
                }
            }
        }
        this.nodeDataSet.update(nodesToUpdate);
    }
    removeColumnNames(){
        let attributes: Node[] = this.nodeDataSet.get(this.network.getConnectedNodes(this.activeNode!.id!) as Id[]) as Node[];
        attributes = attributes.map((node: Node) => { // @ts-ignore
            node.columnName = null; return node})
        // @ts-ignore
        this.activeNode.attribute = attributes;
        this.nodeDataSet.update(attributes);
    }
    handleNodeMetadataChanging(source: any) {
        this.buffered = true;
        console.log(source)
        if (source.name == 'descriptor') {
            this.activeNode!['label'] = source.value;
            // @ts-ignore
            this.activeNode![source.name] = source.value;
        } else {
            if (source.id.includes('vaadin-combo-box')) {
                return;
            }
            if (source.type == 'checkbox') {
                if (source.name == 'timeRange') {
                    // @ts-ignore
                    if (this.activeNode.type == NodeType.TIE) {
                        this.changeTieHistoricity(source.checked);
                    }
                    // @ts-ignore
                    if (this.activeNode.type == NodeType.ATTRIBUTE) {
                        this.changeAttributeHistoricity(source.checked);
                    }
                } else if (source.name == 'knotRange') {
                    this.changeAttributeKnotation(source.checked);
                } else if (source.name == 'knotRole') {
                    this.changeTieKnotation(source.checked);
                } else if (source.name == 'transactional') {
                    this.changeTxProperty(source.checked);
                    this.removeColumnNames();
                } else {
                    // @ts-ignore
                    this.activeNode[source.name] = source.checked;
                }
            } else {
                // @ts-ignore
                if (source.name == 'mnemonic'){
                    // @ts-ignore
                    if(this.activeNode!.type == NodeType.KNOT){
                        this.changeKnotRangeRole(source.value);
                    }
                    // @ts-ignore
                    if(this.activeNode!.type == NodeType.ANCHOR){
                        this.changeAnchorRole(source.value);
                    }
                }
                // @ts-ignore
                this.activeNode[source.name] = source.value;
            }
        }
        this.knots = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.KNOT))
            }
        }).map(node => { // @ts-ignore
            return node.mnemonic});
    }

    changeKnotRangeRole(value: string) {
        let nodes = this.nodeDataSet.get(this.network.getConnectedNodes(this.activeNode!.id!) as IdType[]);
        for (let node of nodes) {
            // @ts-ignore
            if (node['type'] == NodeType.TIE) {
                // @ts-ignore
                node['knotRole'][0].type = value;
            }
            // @ts-ignore
            if (node['type'] == NodeType.ATTRIBUTE) {
                // @ts-ignore
                node['knotRange'] = value;
                this.renewAttributeInAnchor(node);
            }
            node = this.setPositions(node);
        }
        this.nodeDataSet.update(nodes);
    }

    changeAnchorRole(value: string){
        let nodes = this.nodeDataSet.get(this.network.getConnectedNodes(this.activeNode!.id!) as IdType[])   //@ts-ignore
            .filter((node: Node) => (node.type == NodeType.TX_ANCHOR || node.type == NodeType.TIE));
        for (let node of nodes) {
            // @ts-ignore
            for(let anchorRole of node.anchorRole){
                // @ts-ignore
                if(anchorRole.type == this.activeNode!.mnemonic){
                    anchorRole.type = value;
                }
            }
            node = this.setPositions(node);
        }
        this.nodeDataSet.update(nodes);
    }

    removeNodeAndEdge(node: Node[]) {
        // @ts-ignore
        if (this.network.getConnectedEdges(node[0]['id']).length == 1) {
            // @ts-ignore
            this.nodeDataSet.remove(node[0]['id']);
            // @ts-ignore
            this.fixedNodes.splice(this.fixedNodes.indexOf(node[0]['id']), 1);
        }
        let edge: Edge[] = this.edgeDataSet.get({
            filter: (edge) => {
                // @ts-ignore
                return (edge.from == node[0]['id'] && edge.to == this.activeNode.id || edge.to == node[0]['id'] && edge.from == this.activeNode.id);
            }
        })
        this.edgeDataSet.remove(edge[0]['id']!);
    }

    changeTxProperty(checked: boolean) {
        if (this.activeNode != null) {
            if (checked) {
                // @ts-ignore
                this.activeNode['transactional'] = true;
                this.activeNode = this.domainRenderer.renderTxAnchor(this.activeNode!);
            } else {
                // @ts-ignore
                this.activeNode['transactional'] = false;
                this.activeNode = this.domainRenderer.renderAnchor(this.activeNode!);
            }
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update([this.activeNode!]);
            this.renderingComponent!.requestUpdate();
        }
    }
    removeAssociatedIndexes(columnName: string){
        // @ts-ignore
        let newIndexes = [];
        // @ts-ignore
        if (this.activeNode.indexes != null && this.activeNode.indexes.length != 0) {
            // @ts-ignore
            for (let i = 0; i < this.activeNode.indexes[0].index.length; i++) {
                // @ts-ignore
                let flagOfRenew = false;
                // @ts-ignore
                for (let columnIndex = 0; columnIndex < this.activeNode.indexes[0].index[i].columns.column.length; columnIndex++) {
                    // @ts-ignore
                    if (this.activeNode.indexes[0].index[i].columns.column[columnIndex].split(" ")[0] == columnName // @ts-ignore
                        || this.activeNode.indexes[0].index[i].columns.column[columnIndex].split("_")[0] == columnName) {
                        flagOfRenew = true;
                    }
                }
                if (!flagOfRenew) {
                    // @ts-ignore
                    newIndexes.push(this.activeNode.indexes[0].index[i])
                }
            }
            // @ts-ignore
            this.activeNode.indexes[0].index = newIndexes;
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
        }
    }
    changeAttributeKnotation(checked: boolean) {
        if (this.activeNode != null) {
            if (checked) {
                let knot = this.addKnot();
                // @ts-ignore
                this.activeNode['knotRange'] = knot['mnemonic']; //@ts-ignore
                this.edgeDataSet.add(this.fillEdge(this.activeNode.id!.toString(), knot.id));
                // @ts-ignore
                this.activeNode.dataRange = null;
            } else {
                let node: Node[] = this.nodeDataSet.get({
                    filter: (node) => {
                        // @ts-ignore
                        return node['type'] == NodeType.KNOT && node['mnemonic'] == this.activeNode['knotRange'];
                    }
                });
                // @ts-ignore
                this.removeAssociatedIndexes(this.activeNode!['knotRange'] as string);
                this.removeNodeAndEdge(node);
                // @ts-ignore
                this.activeNode['knotRange'] = null;
                // @ts-ignore
                if(this.activeNode.extendedColumn != null && this.activeNode.extendedColumn.length != 0){
                    // @ts-ignore
                    this.activeNode.dataRange = DataRange.BIGINT;
                }
            }
            this.activeNode = this.setPositions(this.activeNode);
            // @ts-ignore
            this.nodeDataSet.update([this.activeNode]);
            this.createAttributeEditorComponent();
        }
        this.knots = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.KNOT))
            }
        }).map(node => { // @ts-ignore
            return node.mnemonic});
    }

    changeTieKnotation(checked: boolean) {
        if (this.activeNode != null) {
            if (checked) {
                let knot = this.addKnot();
                // @ts-ignore
                this.activeNode['knotRole'] = [{
                    description: knot['description'],
                    type: knot['mnemonic'],
                    identifier: false,
                    role: "role"
                }];
                this.edgeDataSet.add(this.fillEdge(this.activeNode.id!.toString(), knot.id, "role, |"));
            } else {
                let node: Node[] = this.nodeDataSet.get({
                    filter: (node) => {
                        // @ts-ignore
                        return node['type'] == NodeType.KNOT && node['mnemonic'] == this.activeNode['knotRole'][0]['type'];
                    }
                });
                // @ts-ignore
                this.removeAssociatedIndexes(this.activeNode['knotRole'][0]['type']);
                this.removeNodeAndEdge(node);
                // @ts-ignore
                this.activeNode['knotRole'] = null;
            }
            this.nodeDataSet.get({
                filter: function (item) {
                    // @ts-ignore
                    return ((item.type == NodeType.KNOT))
                }
            }).map(node => { // @ts-ignore
                return node.mnemonic});
            this.nodeDataSet.update([this.activeNode]);
            this.createTieEditorComponent();
        }
        this.knots = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.KNOT))
            }
        }).map(node => { // @ts-ignore
            return node.mnemonic});

    }

    changeTieHistoricity(checked: boolean) {
        if (this.activeNode != null) {
            if (checked) {
                // @ts-ignore
                this.activeNode['timeRange'] = 'BIGINT';
                this.activeNode = this.domainRenderer.renderHistoricalTie(this.activeNode!);
            } else {
                // @ts-ignore
                this.activeNode['timeRange'] = null;
                this.activeNode = this.domainRenderer.renderTie(this.activeNode!);

                this.removeAssociatedIndexes('changedat');

            }
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update([this.activeNode!]);
            // this.updateMetadataLayout();
            this.renderingComponent?.requestUpdate();
        }
    }

    changeAttributeHistoricity(checked: boolean, timeRange?: string) {
        if (this.activeNode != null) {
            this.activeNode = this.domainRenderer.renderAttribute(this.activeNode!);
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);
            if (checked) {
                // @ts-ignore
                this.activeNode['timeRange'] = 'BIGINT';
                // @ts-ignore
                if (this.activeNode['extendedColumn'] != null && this.activeNode['extendedColumn'] != "") {
                    this.activeNode = this.domainRenderer.renderHistoricalCortegeAttribute(this.activeNode!);
                } else {
                    this.activeNode = this.domainRenderer.renderHistoricalAttribute(this.activeNode!);
                }
            } else {
                // @ts-ignore
                this.activeNode['timeRange'] = null;
                // @ts-ignore
                if (this.activeNode['extendedColumn'] != null && this.activeNode['extendedColumn'] != "") {
                    this.activeNode = this.domainRenderer.renderCortegeAttribute(this.activeNode!);
                } else {
                    this.activeNode = this.domainRenderer.renderAttribute(this.activeNode!);
                }
                this.removeAssociatedIndexes('changedat');
            }
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update([this.activeNode!]);
            // this.updateMetadataLayout();
            this.renderingComponent?.requestUpdate();
        }
    }

    createAnchorEditorComponent() {
        this.renderingComponent = new AnchorEditorLayout();
        this.renderingComponent.setAnchor(this.activeNode);
        this.setKnots();
        this.renderingComponent.render();
        this.initInputEvent();
    }

    createKnotEditorComponent() {
        this.renderingComponent = new KnotEditorLayout();
        this.renderingComponent.setKnot(this.activeNode);
        this.renderingComponent.render();
        this.initInputEvent();
    }

    createAttributeEditorComponent() {
        this.renderingComponent = new AttributeEditorLayout();
        this.renderingComponent.setAttribute(this.activeNode);
        this.setKnots();
        this.renderingComponent.render();
        this.initInputEvent();
    }

    createTxAnchorEditorComponent() {
        this.renderingComponent = new TxAnchorLayout();
        this.renderingComponent.setTxAnchor(this.activeNode);
        this.setKnots();
        this.renderingComponent.render();
        this.initInputEvent();
    }

    setKnots(){
        this.knots = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.KNOT))
            }
        }).map(node => { // @ts-ignore
            return node.mnemonic});
        this.renderingComponent!.setKnots(
            this.knots
        );
    }

    createTieEditorComponent() {
        this.renderingComponent = new TieEditorLayout();
        this.renderingComponent.setTie(this.activeNode);
        this.setKnots();
        this.renderingComponent.render();
        this.initInputEvent();
    }

    updateMetadataLayout(){
        // @ts-ignore
        switch (this.activeNode!.type) {
            case NodeType.ANCHOR:{
                this.createAnchorEditorComponent();
                break
            }
            case NodeType.ATTRIBUTE:{
                this.createAttributeEditorComponent();
                break
            }
            case NodeType.TIE:{
                this.createTieEditorComponent();
                break
            }
            case NodeType.TX_ANCHOR:{
                this.createTxAnchorEditorComponent();
                break
            }
            case NodeType.KNOT:{
                this.createKnotEditorComponent();
                break
            }
        }
    }

    initInputEvent() {

        this.renderingComponent!.addEventListener('click', ev => {
            ev.stopImmediatePropagation();
            if(this.activeFieldName != null && this.activeFieldName != "" && this.buffered){
                this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
                this.activeFieldName = null;
                this.buffered = false;
            }
            // @ts-ignore
            this.activeFieldName = ev.composedPath()[0].name;
        });


        this.renderingComponent!.oninput = (e: Event) => {
            const usedSheet = this.renderingComponent?.getSelectedSheet();
            let source;
            try {
                // @ts-ignore
                this.handleNodeMetadataChanging(e.path[0]);
                // @ts-ignore
                source = e.path[0];
            } catch (err) {
                // @ts-ignore
                this.handleNodeMetadataChanging(e.originalTarget);
                // @ts-ignore
                source = e.originalTarget;
            }
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);

            // @ts-ignore
            if(this.activeNode.type == NodeType.ATTRIBUTE){
                this.renewAttributeInAnchor();
            }

            if (source.type == 'checkbox'){
                this.buffered = false;
                this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
                this.updateMetadataLayout();
            } else{
                this.renderingComponent?.requestUpdate();
            }
            this.renderingComponent!.setKnots(this.knots);
            this.renderingComponent?.setSelectedSheet(usedSheet as number);

        }
        this.renderingComponent!.addEventListener('add-item-from-dialog', (e: Event) => {
            // @ts-ignore
            this.handleItemAdding(e.detail);
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            this.renderingComponent?.requestUpdate();
            this.renderingComponent?.setSelectedSheet(SheetType.INDEX_SHEET);
        });
        this.renderingComponent!.addEventListener('add-index-from-dialog', (e: Event) => {
            // @ts-ignore
            this.handleIndexAdding(e.detail);
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            // this.updateMetadataLayout();
            this.renderingComponent?.requestUpdate();
            this.renderingComponent?.setSelectedSheet(SheetType.INDEX_SHEET);
        });
        this.renderingComponent!.addEventListener('delete-knot-item', (e: Event) => {
            // @ts-ignore
            this.handleKnotItemDeleting(e.detail);
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            this.renderingComponent?.requestUpdate();
            this.renderingComponent?.setSelectedSheet(SheetType.INDEX_SHEET);
        });
        this.renderingComponent!.addEventListener('delete-index', (e: Event) => {
            // @ts-ignore
            this.handleIndexDeleting(e.detail);
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            this.renderingComponent?.requestUpdate();
            this.renderingComponent?.setSelectedSheet(SheetType.INDEX_SHEET);
        });
        this.renderingComponent!.addEventListener('delete-column', (e: Event) => {
            const usedSheet = this.renderingComponent?.getSelectedSheet();
            // @ts-ignore
            this.handleExtendedColumnDeleting(e.detail);
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            this.renderingComponent?.requestUpdate();
            this.renderingComponent?.setSelectedSheet(usedSheet as number);
        });
        this.renderingComponent!.addEventListener('add-column-from-dialog', (e: Event) => {
            const usedSheet = this.renderingComponent?.getSelectedSheet();
            e.stopImmediatePropagation();
            // @ts-ignore
            this.handleColumnAdding(e.detail.column);
            this.activeNode = this.setPositions(this.activeNode);
            // this.activeNode = this.domainRenderer.renderCortegeAttribute(this.activeNode!);
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            this.updateMetadataLayout();
            this.renderingComponent?.setSelectedSheet(usedSheet as number);
        });
        this.renderingComponent!.addEventListener('combo-box-changed', (e: Event) => {
            const usedSheet = this.renderingComponent?.getSelectedSheet();
            // @ts-ignore
            if (e.detail.cBoxTie) {
                // @ts-ignore
                if (e.detail.value == "" || e.detail.value == null) {
                    // @ts-ignore
                    this.activeNode['timeRange'] = null;
                    this.activeNode = this.domainRenderer.renderTie(this.activeNode!);
                    this.removeAssociatedIndexes('changedat');
                } else {
                    // @ts-ignore
                    this.activeNode['timeRange'] = e.detail.value;
                    this.activeNode = this.domainRenderer.renderHistoricalTie(this.activeNode!);
                }   // @ts-ignore
            } else if(e.detail.cBoxIdentity != null && e.detail.cBoxIdentity) {
                // @ts-ignore
                if (e.detail.value == "" || e.detail.value == null) {
                    // @ts-ignore
                    this.activeNode['identity'] = 'BIGINT';
                    // @ts-ignore
                } else {
                    // @ts-ignore
                    this.activeNode['identity'] = e.detail.value;
                } //@ts-ignore
            } else if(e.detail.cBoxDataRange != null && e.detail.cBoxDataRange){
                // @ts-ignore
                this.activeNode['dataRange'] = e.detail.value; // @ts-ignore
                if (e.detail.value != null && e.detail.value != "" && this.activeNode.knotRange != null && this.activeNode.knotRange != "") {
                    this.changeAttributeKnotation(false);
                }
            } else {
                this.activeNode = this.domainRenderer.renderAttribute(this.activeNode!);
                this.activeNode = this.setPositions(this.activeNode);
                this.nodeDataSet.update(this.activeNode!);
                // @ts-ignore
                if (e.detail.value == "" || e.detail.value == null) {
                    // @ts-ignore
                    this.activeNode['timeRange'] = null;
                    // @ts-ignore
                    if (this.activeNode['extendedColumn'] != null && this.activeNode['extendedColumn'] != "") {
                        this.activeNode = this.domainRenderer.renderCortegeAttribute(this.activeNode!);
                    } else {
                        this.activeNode = this.domainRenderer.renderAttribute(this.activeNode!);
                    }
                    this.removeAssociatedIndexes('changedat');
                } else {
                    // @ts-ignore
                    this.activeNode['timeRange'] = e.detail.value;
                    // @ts-ignore
                    if (this.activeNode['extendedColumn'] != null && this.activeNode['extendedColumn'] != "") {
                        this.activeNode = this.domainRenderer.renderHistoricalCortegeAttribute(this.activeNode!);
                    } else {
                        this.activeNode = this.domainRenderer.renderHistoricalAttribute(this.activeNode!);
                    }
                }
            } // @ts-ignore
            if(this.activeNode!.type == NodeType.ATTRIBUTE){
                this.renewAttributeInAnchor();
            }
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            // this.renderingComponent?.requestUpdate();
            this.updateMetadataLayout();
            this.renderingComponent?.setSelectedSheet(usedSheet as number);
        });
    }

    handleItemAdding(details: any) {
        // @ts-ignore
        if (this.activeNode.values != null && this.activeNode.values[0].value != null && this.activeNode.values[0].value.length != 0) {
            if (details.id != null && details.id != "") {
                // @ts-ignore
                const indexOfUpdatingObject = this.activeNode.values[0].value.findIndex(o => o.id == details.id);
                if(indexOfUpdatingObject == -1){
                    // @ts-ignore
                    this.activeNode.values[0].value.push({id: details.id, value: details.value});
                } else {
                    // @ts-ignore
                    this.activeNode.values[0].value[indexOfUpdatingObject] = details;
                }
            } else {
                // @ts-ignore
                let maxId = Math.max(...this.activeNode.values[0].value.map(o => o.id));
                // @ts-ignore
                this.activeNode.values[0].value.push({id: ++maxId, value: details.value});
            }
        } else {
            if(details.id == "" || details.id == null){
                details.id = 0;
            }
            // @ts-ignore
            this.activeNode.values = [{
                value: [
                    {
                        id: details.id,
                        value: details.value
                    }
                ]
            }]
        }
    }

    handleIndexAdding(details: any) {
        // @ts-ignore
        if (this.activeNode.indexes != null && this.activeNode.indexes[0].index.length != 0) {
            if (details.id != null) {
                // @ts-ignore
                const indexOfUpdatingObject = this.activeNode.indexes[0].index.findIndex(o => o.id == details.id);
                // @ts-ignore
                this.activeNode.indexes[0].index[indexOfUpdatingObject] = details;
            } else {
                details.id = ++this.indexId;
                // @ts-ignore
                this.activeNode.indexes[0].index.push(details);
            }
        } else {
            details.id = ++this.indexId;
            // @ts-ignore
            this.activeNode.indexes = [{
                index: [
                    details
                ]
            }]
        }

        // @ts-ignore
        if(this.activeNode.type == NodeType.ATTRIBUTE){
            this.renewAttributeInAnchor();
        }
    }

    renewAttributeInAnchor(attribute?: Node){
        let anchor = null;
        if(attribute == null){ // @ts-ignore
            anchor = this.nodeDataSet.get(this.network.getConnectedNodes(this.activeNode!.id!) as Id[]).filter(node => node.type == NodeType.ANCHOR || node.type == NodeType.TX_ANCHOR)[0] as Node
            // @ts-ignore
            anchor.attribute[anchor.attribute.findIndex((obj: Node) => obj.id == this.activeNode!.id)] = this.activeNode;
        } else {
            // @ts-ignore
            anchor = this.nodeDataSet.get(this.network.getConnectedNodes(attribute?.id) as Id[]).filter(node => node.type == NodeType.ANCHOR)[0] as Node
            // @ts-ignore
            anchor.attribute[anchor.attribute.findIndex((obj: Node) => obj.id == attribute?.id)] = attribute;

        }
        anchor = this.setPositions(anchor);
        this.nodeDataSet.update(anchor);
        this.renderingComponent?.requestUpdate();
    }

    handleKnotItemDeleting(details: any) {
        // @ts-ignore
        if (details.id != null) {
            // @ts-ignore
            const indexOfUpdatingObject = this.activeNode.values[0].value.findIndex(o => o.id == details.id);
            // @ts-ignore
            this.activeNode.values[0].value.splice(indexOfUpdatingObject, 1);
        }
    }

    handleIndexDeleting(details: any) {
        // @ts-ignore
        if (details.id != null) {
            // @ts-ignore
            if(this.activeNode.indexes != null){
                // @ts-ignore
                const indexOfUpdatingObject = this.activeNode.indexes[0].index.findIndex(o => o.id == details.id);
                // @ts-ignore
                this.activeNode.indexes[0].index.splice(indexOfUpdatingObject, 1);
            }
        }
    }

    handleExtendedColumnDeleting(details: any) {
        // @ts-ignore
        if (details.id != null) {
            // @ts-ignore
            const indexOfUpdatingObject = this.activeNode.extendedColumn.findIndex(o => o.id == details.id);
            // @ts-ignore
            const columnName = this.activeNode.extendedColumn[indexOfUpdatingObject].name;
            // @ts-ignore
            this.activeNode.extendedColumn.splice(indexOfUpdatingObject, 1);
            this.removeAssociatedIndexes(columnName);
        }

        // @ts-ignore
        if (this.activeNode.extendedColumn.length == 0 && this.activeNode.type == NodeType.ATTRIBUTE) {
            this.activeNode = this.domainRenderer.renderAttribute(this.activeNode!);
            this.activeNode = this.setPositions(this.activeNode);
            this.nodeDataSet.update(this.activeNode!);

            // @ts-ignore
            if (this.activeNode.timeRange != null && this.activeNode.timeRange != "") {
                this.activeNode = this.domainRenderer.renderHistoricalAttribute(this.activeNode!);
            } else {
                // @ts-ignore
                this.activeNode = this.domainRenderer.renderAttribute(this.activeNode!);
            }

        }

        this.nodeDataSet.update(this.activeNode!);

    }

    handleColumnAdding(details: any) {
        ++this.indexId;
        // @ts-ignore
        // if(this.activeNode.type == NodeType.TX_ANCHOR || this.activeNode.type == NodeType.TIE){
        //     details.id = ++this.indexId;
        //     // @ts-ignore
        //     if(this.activeNode!.indexes != null && this.activeNode!.indexes.length != 0 && this.activeNode!.extendedColumn != null && this.activeNode!.extendedColumn.length != 0){
        //         // @ts-ignore
        //         for(let index of this.activeNode!.indexes[0].index){
        //             for(let columnIndex = 0; columnIndex < index.columns.column.length; columnIndex++){ // @ts-ignore
        //                 if(index.columns.column[columnIndex].includes(structuredClone(this.activeNode!.extendedColumn[0]).name)){
        //                     index.columns.column[columnIndex] = details.name + " " + index.columns.column[columnIndex].split(" ")[1];
        //                 }
        //             }
        //         }
        //     }
        //     // @ts-ignore
        //     this.activeNode!.extendedColumn = [details]
        // } else {
            //@ts-ignore
            if (this.activeNode.extendedColumn != null && this.activeNode.extendedColumn.length != 0) {
                if (details.id != null) {
                    // @ts-ignore
                    const indexOfUpdatingObject = this.activeNode.extendedColumn.findIndex(o => o.id == details.id);
                    // @ts-ignore
                    if(this.activeNode.indexes != null && this.activeNode.indexes.length != 0){ // @ts-ignore
                        for(let index of this.activeNode!.indexes[0].index){
                            for(let columnIndex = 0; columnIndex < index.columns.column.length; columnIndex++){ // @ts-ignore
                                if(index.columns.column[columnIndex].includes(structuredClone(this.activeNode.extendedColumn[indexOfUpdatingObject]).name)){
                                    index.columns.column[columnIndex] = details.name + " " + index.columns.column[columnIndex].split(" ")[1];
                                }
                            }
                        }
                    }
                    // @ts-ignore
                    this.activeNode.extendedColumn[indexOfUpdatingObject] = details;
                } else {
                    details.id = ++this.indexId;
                    // @ts-ignore
                    this.activeNode.extendedColumn.push(details);
                }
            } else {
                details.id = ++this.indexId;
                // @ts-ignore
                this.activeNode.extendedColumn = [details]
            }
        // }
        this.activeNode = this.setPositions(this.activeNode);
        this.nodeDataSet.update(this.activeNode!);
        // @ts-ignore
        if (this.activeNode.type == NodeType.ATTRIBUTE) {
            this.activeNode = this.domainRenderer.renderAttribute(this.activeNode!);
            this.nodeDataSet.update(this.activeNode)
            // @ts-ignore
            if (this.activeNode.timeRange != null && this.activeNode.timeRange != "") {
                this.activeNode = this.domainRenderer.renderHistoricalCortegeAttribute(this.activeNode!);
            } else {
                this.activeNode = this.domainRenderer.renderCortegeAttribute(this.activeNode!);
            }
            this.nodeDataSet.update(this.activeNode!);
            this.renewAttributeInAnchor();
        }
    }

    getSelectedNode() {
        let nodeId = this.network.getSelectedNodes();
        let node: Node = this.nodeDataSet.get(nodeId)[0]
        return node;
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        this.initTree();
        this.$server?.fillComponentRequest();
        this.lastId = 1000;
        this.network.moveTo({position: {x: 2000, y: 2000}});
    }

    initTree() {
        const data = {nodes: this.nodeDataSet, edges: this.edgeDataSet,};
        // const options = {
        //     width: '100%',
        //     interaction: {
        //         keyboard: true,
        //         multiselect: true
        //     },
        //     physics: {
        //         enabled: true,
        //         stabilization: {
        //             enabled: false
        //         }
        //     },
        //     edges: {
        //         arrows: {
        //             to: {enabled: false, scaleFactor: 0.6, type: 'arrow'}
        //         }
        //     }
        // };
        const options = {
            "edges": {
                "smooth": {
                    "forceDirection": "none"
                }
            },
            "physics": {
                "barnesHut": {
                    "gravitationalConstant": -1700,
                    "centralGravity": 0,
                    "springLength": 235,
                    "damping": 0.15,
                    "avoidOverlap": 5
                },
                "maxVelocity": 21,
                "minVelocity": 0.75,
                "timestep": 0.72
            },
            "interaction": {
                "keyboard": true,
                "multiselect": true
            },
        } // @ts-ignore
        this.network = new Network(this.shadowRoot!.getElementById("customId")!, data, options);
        this.network.moveTo({scale: 0.25})
    }


    setPositions(node: any) {
        let nodePosition = this.network.getPositions([node['id']]);
        node['x'] = nodePosition[node['id']].x;
        node['y'] = nodePosition[node['id']].y;
        return node;
    }

    addKnot() {
        let node = createKnotNodeTemplate(this.activeNode!);
        node = fillKnotFigure(node);
        this.nodeDataSet.add([node]);
        this.knots = this.nodeDataSet   // @ts-ignore
            .get({filter: function (item) {return ((item.type == NodeType.KNOT))}}) // @ts-ignore
            .map(node => { return node.mnemonic});
        return node;
    }

    handleZoom(e: CustomEvent) {
        if (e.detail == 'zoom-plus') {
            if (this.network.getScale() * 1.3 <= 1.5) {
                this.scale = this.scale * 1.3;
                this.network.moveTo({scale: this.scale});
            }
        } else {
            if (this.network.getScale() / 1.3 >= 0.2) {
                this.scale = this.scale / 1.3;
                this.network.moveTo({scale: this.scale});
            }
        }
    }

    handleDownload(e: CustomEvent) {
        this.download();
    }

    handleSearch(e: CustomEvent) {
        try {
            switch (e.detail.length) {
                case 2: {

                    break;
                }
            }
        } catch (e) {
        }
    }

    handlePin(e: CustomEvent) {
        if (this.pinAll) {
            this.nodeDataSet.forEach(node => {
                if (!this.fixedNodes.includes(node.id as string)) {
                    this.nodeDataSet.update({id: node.id, fixed: true});
                    this.fixedNodes.push(node.id as string)
                }
            });

            this.pinAll = false;
        } else {
            this.nodeDataSet.forEach(node => {
                if (this.fixedNodes.includes(node.id as string)) {
                    this.nodeDataSet.update({id: node.id, fixed: false});
                    this.fixedNodes.splice(this.fixedNodes.indexOf(node.id as string), 1);
                }
            });
            this.pinAll = true;
        }
    }

    addAnchor() {
        let node = createAnchorNodeTemplate(this.activeNode!);
        node = fillAnchorFigure(node);
        this.nodeDataSet.add([node]);
        this.connectNodes(node);
        this.chosenNodeType = NodeType.NO_TYPE;
        this.network.unselectAll();
        return node;
    }

    addTxAnchor() {
        let node = createTxAnchorNodeTemplate(++this.lastId, this.activeNode!);
        node = fillTxAnchorFigure(node);
        this.addTieToLayoutProperties(node);
        this.fillAnchorKnotRoleByTie(node);
        return node;
    }

    addTieToLayoutProperties(node: object) {
        this.nodeDataSet.add([node]);
        this.connectNodes(node);
        this.chosenNodeType = NodeType.NO_TYPE;
        this.network.unselectAll();
    }

    addTie() {
        let node = createTieNodeTemplate(++this.lastId, this.activeNode!);
        node = fillTieFigure(node);
        this.addTieToLayoutProperties(node);
        this.fillAnchorKnotRoleByTie(node);
        return node;
    }

    addSelfTie() {
        let node = createTieNodeTemplate(++this.lastId, this.activeNode!);
        node = fillTieFigure(node);
        this.nodeDataSet.add(node as unknown as Node);
        this.edgeDataSet.add([this.fillEdge(node['id'], this.activeNode!.id!.toString()),
        this.fillEdge(node['id'], this.activeNode!.id!.toString())]);
        this.fillAnchorKnotRoleByTie(node);
        this.chosenNodeType = NodeType.NO_TYPE;
        this.network.unselectAll();
    }

    addHistoricalTie() {
        let node = createTieNodeTemplate(++this.lastId, this.activeNode!);
        // @ts-ignore
        node['timeRange'] = 'BIGINT';
        node = fillTieFigure(node);
        this.addTieToLayoutProperties(node);
        this.fillAnchorKnotRoleByTie(node);
        return node;
    }

    addAnchoredTie() {
        let tie = this.addTie();
        let anchor = this.addAnchor();
        let edge = this.fillEdge(tie['id'], anchor['id'], "example role, |");
        this.edgeDataSet.add([edge]);
        this.fillAnchorKnotRoleByTie(tie);
    }

    fillAnchorKnotRoleByTie(tie: any){

        for(let edgeId of this.network.getConnectedEdges(tie.id)){
            const edge = this.edgeDataSet.get(edgeId);
            if((edge!.to! as string).length == 2){
                const childNode = this.nodeDataSet.get(edge!.to as string);
                const nodeToPush = {
                    'role': edge?.label?.split(", ")[0],
                    // @ts-ignore
                    'type': childNode['mnemonic'],
                    'description': null,
                    'identifier': edge?.label?.split(", ")[0] == '|',
                    'id': edgeId
                }
                if(tie.anchorRole != null){
                    // @ts-ignore
                    if(tie.anchorRole?.filter((anchorRole: { type: any; }) => anchorRole.type == childNode.mnemonic).length != 0){
                        continue;
                    }
                    tie.anchorRole.push(nodeToPush);
                } else {
                    tie.anchorRole = [nodeToPush]
                }
            }
        }
        this.nodeDataSet.update(tie)
    }

    fillAnchorKnotRoleByAnchor(anchor: any){
        for(let nodeId of this.network.getConnectedNodes(anchor.id)){
            let childNode = this.nodeDataSet.get(nodeId as IdType);
            // @ts-ignore
            if(childNode.type == NodeType.TIE || childNode.type == NodeType.TX_ANCHOR){ //@ts-ignore
                let edge: Edge = this.edgeDataSet.get(this.network.getConnectedEdges(anchor.id)).filter((edge: Edge) => edge.to == anchor.id && edge.from == childNode?.id)[0];
                const template = {
                    description: "descExample",
                    role: "roleExample",            // @ts-ignore
                    type: anchor.mnemonic,
                    identifier: false,
                    id: edge.id
                }; // @ts-ignore
                edge.label = 'roleExample, |'; // @ts-ignore
                if(childNode.type == NodeType.TX_ANCHOR){
                    edge.arrows = "to";
                } // @ts-ignore
                if(childNode.anchorRole == null || childNode.anchorRole.length == 0){ // @ts-ignore
                    childNode.anchorRole = [template]
                } else { // @ts-ignore
                    childNode.anchorRole.push(template);
                }
                this.edgeDataSet.update([edge]);
                childNode = this.setPositions(childNode);
            }
            this.nodeDataSet.update(childNode!)
        }
    }

    addAnchoredHistoricalTie() {
        let tie = this.addHistoricalTie();
        let anchor = this.addAnchor();
        let edge = this.fillEdge(tie['id'], anchor['id'], "example role, |");
        this.edgeDataSet.add([edge]);
        this.fillAnchorKnotRoleByTie(tie);
    }


    addAttribute() {
        let node = createAttributeNodeTemplate(this.network, this.activeNode!);
        // @ts-ignore
        node['extendedColumn'] = [];
        node = fillAttributeFigure(node, ++this.indexId);
        this.addAttributeToLayoutProperties(node);
        return node;
    }

    addHistoricalAttribute() {
        let node = createAttributeNodeTemplate(this.network, this.activeNode!);
        // @ts-ignore
        node['timeRange'] = 'BIGINT';
        // @ts-ignore
        node['extendedColumn'] = [];
        node = fillAttributeFigure(node, ++this.indexId);
        this.addAttributeToLayoutProperties(node);
        return node;
    }

    addComposedAttribute() {
        let node = createAttributeNodeTemplate(this.network, this.activeNode!);
        // @ts-ignore
        node['extendedColumn'] = [
            {
                name: 'colNameExample',
                dataRange: 'BIGINT',
                knotRange: null,
                description: 'descExample',
                descriptor: 'descriptorExample',
                length: 512,
                id: ++this.indexId
            }
        ];
        node = fillAttributeFigure(node, ++this.indexId);
        this.addAttributeToLayoutProperties(node);
        return node;
    }

    addAttributeToLayoutProperties(node: Node) {
        this.nodeDataSet.add([node]);
        this.connectNodes(node);
        this.chosenNodeType = NodeType.NO_TYPE;
        this.network.unselectAll();
    }

    connectAnchorToTie(){
        // @ts-ignore
        const anchorId = this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type == NodeType.ANCHOR)[0];
        // @ts-ignore
        const tieId =  this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type == NodeType.TIE || this.nodeDataSet.get(node)!.type == NodeType.TX_ANCHOR)[0];

        let anchor = this.nodeDataSet.get(anchorId);
        let tie = this.nodeDataSet.get(tieId); // @ts-ignore
        let edge = this.fillEdge(tie!['id'] as string, anchorId as string, "roleExample, |", tie.type == NodeType.TX_ANCHOR);
        const template = {
            description: "descExample",
            role: "roleExample",            // @ts-ignore
            type: anchor.mnemonic,
            identifier: false,
            id: edge.id
        }; // @ts-ignore
        if(tie.anchorRole == null || tie.anchorRole.length == 0){ // @ts-ignore
            tie.anchorRole = [template]
        } else { // @ts-ignore
            tie.anchorRole.push(template);
        }
        tie = this.setPositions(tie);
        this.nodeDataSet.update(tie as Node);
        this.edgeDataSet.add([edge]);
    }

    connectKnotWithTieOrAttribute(){        // @ts-ignore
        const knotId = this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type == NodeType.KNOT)[0];     // @ts-ignore
        const tieId =  this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type == NodeType.TIE);     // @ts-ignore
        const attributeId =  this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type == NodeType.ATTRIBUTE);
        // @ts-ignore
        const knotMnemonic  = this.nodeDataSet.get(knotId).mnemonic;
        if(tieId.length != 0){
            let tie = this.nodeDataSet.get(tieId[0]);
            // @ts-ignore
            tie['knotRole'] = [{
                description: "descExample",
                role: "roleExample",
                type: knotMnemonic,
                identifier: false
            }]
            tie = this.setPositions(tie);
            this.nodeDataSet.update(tie as Node);
            let edge = this.fillEdge(tie!['id'] as string, knotId as string);
            edge.label = "roleExample, |"
            this.edgeDataSet.add([edge]);
        } else {
            let attribute = this.nodeDataSet.get(attributeId[0]);
            // @ts-ignore
            attribute['knotRange'] = knotMnemonic;
            attribute = this.setPositions(attribute);
            this.nodeDataSet.update(attribute as Node);
            let edge = this.fillEdge(attribute!['id'] as string, knotId as string);
            this.edgeDataSet.add([edge]);
            this.renewAttributeInAnchor(attribute as Node);
        }
    }

    connectNodes(node: any) {
        for (let i = 0; i < this.network.getSelectedNodes().length; i++) {
            try {
                let edge = null;
                switch (node.type) {
                    case NodeType.TIE: {
                        edge = this.fillEdge(node['id'], this.network.getSelectedNodes()[i].toString(),'example role, |');
                        break;
                    }
                    case NodeType.TX_ANCHOR: {
                        edge = this.fillEdge(node['id'], this.network.getSelectedNodes()[i].toString(),'example role, |', true);
                        break;
                    }
                    default: {
                        edge = this.fillEdge(this.network.getSelectedNodes()[i].toString(), node['id']);
                        break;
                    }
                }
                // @ts-ignore
                this.edgeDataSet.add([edge]);
                let parentNode = this.nodeDataSet.get(this.network.getSelectedNodes()[i]);
                // @ts-ignore
                if ((parentNode.type == NodeType.ANCHOR || parentNode.type == NodeType.TX_ANCHOR) && node.type == NodeType.ATTRIBUTE) {
                    // @ts-ignore
                    if (parentNode.attribute != null) {
                        // @ts-ignore
                        parentNode.attribute.push(node);
                    } else {
                        // @ts-ignore
                        parentNode.attribute = [node];
                    }
                }
            } catch (err) {
                console.log(err)
            }
        }
        if (node['type'] != NodeType.KNOT && node['type'] != NodeType.ATTRIBUTE) {
            this.network.unselectAll();
        }
    }


    noteNodeAsOld(node: Node){
        // @ts-ignore
        // node['oldMnemonic'] = node['mnemonic'];
        // @ts-ignore
        node['isNew'] = false;
        node = this.setPositions(node);
        return node;
    }

    download() {

        let newKnots: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.KNOT && item.isNew != null && item.isNew == true))
            }
        });
        let oldKnots: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.KNOT && (item.isNew == null || item.isNew == false)))
            }
        });

        let newAnchors: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.ANCHOR && item.isNew != null && item.isNew == true))
            }
        });
        let oldAnchors: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.ANCHOR && (item.isNew == null || item.isNew == false)))
            }
        });

        let newTies: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.TIE && item.isNew != null && item.isNew == true))
            }
        });
        let oldTies: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.TIE && (item.isNew == null || item.isNew == false)))
            }
        });

        let newTxAnchors: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.TX_ANCHOR && item.isNew != null && item.isNew == true))
            }
        });
        let oldTxAnchors: Node[] = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.TX_ANCHOR && (item.isNew == null || item.isNew == false)))
            }
        });

        this.$server?.addKnot(JSON.stringify(newKnots.map(node => mapNodeToKnot(node))));
        this.$server?.updateKnot(JSON.stringify(oldKnots.map(node => mapNodeToKnot(node))));
        this.$server?.deleteKnot(this.knotsToDelete.join(','));

        this.$server?.addAnchor(JSON.stringify(newAnchors.map(node => mapNodeToAnchor(node))));
        this.$server?.updateAnchor(JSON.stringify(oldAnchors.map(node => mapNodeToAnchor(node))));
        this.$server?.deleteAnchor(this.anchorsToDelete.join(','));

        this.$server?.addTie(JSON.stringify(newTies.map(node => mapNodeToTie(node))));
        this.$server?.updateTie(JSON.stringify(oldTies.map(node => mapNodeToTie(node))));
        this.$server?.deleteTie(this.tiesToDelete.join(','));

        this.$server?.addTxAnchor(JSON.stringify(newTxAnchors.map(node => mapNodeToTxAnchor(node))));
        this.$server?.updateTxAnchor(JSON.stringify(oldTxAnchors.map(node => mapNodeToTxAnchor(node))));
        this.$server?.deleteTxAnchor(this.txAnchorsToDelete.join(','));


        newKnots = newKnots.map(node => this.noteNodeAsOld(node));
        oldKnots = oldKnots.map(node => this.noteNodeAsOld(node));
        newAnchors = newAnchors.map(node => this.noteNodeAsOld(node));
        oldAnchors = oldAnchors.map(node => this.noteNodeAsOld(node));
        newTies = newTies.map(node => this.noteNodeAsOld(node));
        oldTies = oldTies.map(node => this.noteNodeAsOld(node));
        newTxAnchors = newTxAnchors.map(node => this.noteNodeAsOld(node));
        oldTxAnchors = oldTxAnchors.map(node => this.noteNodeAsOld(node));
        // ties = ties.map(node => this.noteNodeAsOld(node));
        // txAnchors = txAnchors.map(node => this.noteNodeAsOld(node));


        this.nodeDataSet.update(newKnots);
        this.nodeDataSet.update(oldKnots);
        this.nodeDataSet.update(newAnchors);
        this.nodeDataSet.update(oldAnchors);
        this.nodeDataSet.update(newTies);
        this.nodeDataSet.update(oldTies);
        this.nodeDataSet.update(newTxAnchors);
        this.nodeDataSet.update(oldTxAnchors);
        // this.nodeDataSet.update(ties);
        // this.nodeDataSet.update(txAnchors);


        this.$server?.marshall();

    };

    allNodesIsTypedAs(nodeType: any) {
        //@ts-ignore
        let array = this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type != nodeType);
        return array.length == 0;
    }
    isKnotThere(){
        // @ts-ignore
        let array = this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type == NodeType.KNOT);
        if (array.length != 0){
            let attribute = this.network.getSelectedNodes().filter(node => {const nd = this.nodeDataSet.get(node)!;  // @ts-ignore
                return nd.type == NodeType.ATTRIBUTE && (nd.knotRange == null || nd.knotRange == "")});
            if(attribute.length != 0){
                return true;
            } else {
                attribute = this.network.getSelectedNodes().filter(node => {const nd = this.nodeDataSet.get(node)!;  // @ts-ignore
                    return nd.type == NodeType.TIE && (nd.knotRole == null || nd.knotRole.length == 0)});
                if(attribute.length != 0){
                    return true;
                }
            }
        }
        return false;
    }
    isAnchorAndTie(){
        // @ts-ignore
        let array = this.network.getSelectedNodes().filter(node => this.nodeDataSet.get(node)!.type == NodeType.ANCHOR);
        if (array.length != 0){
            let tie = this.network.getSelectedNodes().filter(node => {const nd = this.nodeDataSet.get(node)!;  // @ts-ignore
                return nd.type == NodeType.TIE || nd.type == NodeType.TX_ANCHOR});
            if(tie.length != 0) {
                return true;
            }
        }
        return false;
    }
    // Точка старта на фронт приложения. Бэк вызывает этот метод, передавая в него узлы.
    getTree(anchors: string, knots: string, ties: string, txAnchors: string) {
        this.nodeDataSet.add(this.getKnots(knots));
        this.nodeDataSet.add(this.getAnchors(anchors));
        this.nodeDataSet.add(this.getTies(ties));
        this.nodeDataSet.add(this.getTxAnchors(txAnchors));

        this.edgeDataSet.add(this.edgeList)

        this.knots = this.nodeDataSet.get({
            filter: function (item) {
                // @ts-ignore
                return ((item.type == NodeType.KNOT))
            }
        }).map(node => { // @ts-ignore
            return node.mnemonic});

        this.history = initHistory(this.edgeDataSet.get(), this.nodeDataSet.get());

        this.initEvents();
    }

    getTxAnchors(txAnchors: string){
        let txAnchorList = JSON.parse(txAnchors);
        let attributeContainer: any = []
        txAnchorList = txAnchorList.map((node: object) => this.parseTxAnchor(node, attributeContainer));
        let bufferList: any[] = []
        for(let list of txAnchorList){
            bufferList = bufferList.concat([list[0]])
            bufferList = bufferList.concat(list[1])
        }
        return bufferList
    }

    parseTxAnchor(node: object, attributeContainer: any[]){
        node = parseNodeMainData(randomBytes(2).toString('hex').substring(0, 2), ++this.indexId, node);
        node = parseNodeLayout(node);
        // @ts-ignore
        if(node.fixed){
            // @ts-ignore
            this.fixedNodes.push(node['id']);
        }
        node = fillTxAnchorFigure(node);
        this.getConnections(node);
        attributeContainer = this.getAttributes(node);
        return [node, attributeContainer];
    }


    getAnchors(anchors: string) {
        let anchorList = JSON.parse(anchors);
        let attributeContainer: any = []
        anchorList = anchorList.map((node: object) => this.parseAnchor(node, attributeContainer));
        let bufferList: any[] = []
        for(let list of anchorList){
            bufferList = bufferList.concat([list[0]])
            bufferList = bufferList.concat(list[1])
        }
        return bufferList
    }

    parseAnchor(node: object, attributeContainer: any[]) {
        node = parseNodeMainData(++this.lastId, ++this.indexId, node);
        node = parseNodeLayout(node);
        // @ts-ignore
        if(node.fixed){
            // @ts-ignore
            this.fixedNodes.push(node['id']);
        }
        node = fillAnchorFigure(node);
        attributeContainer = this.getAttributes(node);
        return [node, attributeContainer];
    }

    getAttributes(anchor: object | any) {
        let attributes = [];
        for (let attributeIndex in anchor['attribute']) {
            let attribute = anchor['attribute'][attributeIndex];
            attribute = this.parseAttribute(attribute, anchor);
            this.fillEdge(anchor['id'], attribute['id']);
            attributes.push(attribute);
        }
        return attributes;
    }

    parseAttribute(node: object, anchor: object) {
        node = parseNodeMainData(++this.lastId, ++this.indexId,node, anchor);
        node = parseNodeLayout(node, anchor);
        // @ts-ignore
        if(node.fixed){
            // @ts-ignore
            this.fixedNodes.push(node['id']);
        }
        node = fillAttributeFigure(node, ++this.indexId);
        this.getKnot(node);
        return node;
    }

    getKnot(attribute: object | any) {
        if (attribute['knotRange'] != null) {
            this.fillEdge(attribute['id'], attribute['knotRange']);
        }
    }

    getKnots(knots: string) {
        let knotList = JSON.parse(knots);
        knotList = knotList.map((node: object) => this.parseKnot(node));
        return knotList
    }

    parseKnot(node: object) {
        node = parseNodeMainData(++this.lastId, ++this.indexId,node);
        node = parseNodeLayout(node);
        // @ts-ignore
        if(node.fixed){
            // @ts-ignore
            this.fixedNodes.push(node['id']);
        }
        node = fillKnotFigure(node);
        return node;
    }

    getTies(ties: string) {
        let tieList = JSON.parse(ties);
        tieList = tieList.map((node: object) => this.parseTie(node));
        return tieList;
    }

    parseTie(node: object) {
        node = parseNodeMainData(++this.lastId, ++this.indexId,node);
        node = parseNodeLayout(node);
        // @ts-ignore
        if(node.fixed){
            // @ts-ignore
            this.fixedNodes.push(node['id']);
        }
        node = fillTieFigure(node);
        this.getConnections(node);
        return node;
    }

    getConnections(tieOrTxAnchor: object | any) {
        for (let anchorIndex in tieOrTxAnchor['anchorRole']) {
            let anchorRole = tieOrTxAnchor['anchorRole'][anchorIndex];
            const count = anchorRole['identifier'] == true ? ", |||" : ", |"
            const edge = this.fillEdge(tieOrTxAnchor['id'], anchorRole['type'], anchorRole['role'] + count, tieOrTxAnchor.type == NodeType.TX_ANCHOR);
            tieOrTxAnchor['anchorRole'][anchorIndex]['id'] = edge.id;
        }
        if (tieOrTxAnchor['knotRole'] != null) {
            tieOrTxAnchor['knotRole'] = [tieOrTxAnchor['knotRole']];
            const count = tieOrTxAnchor['knotRole'][0]['identifier'] == true ? ", |||" : ", |"
            const edge = this.fillEdge(tieOrTxAnchor['id'], tieOrTxAnchor['knotRole'][0]['type'], tieOrTxAnchor['knotRole'][0]['role'] + count);
            tieOrTxAnchor['knotRole'][0]['id'] = edge.id;
        }
    }

    fillEdge(idFrom: string, idTo: string, label?: string, arrow?: boolean) {
        let edge : Edge = {
            'from': idFrom,
            'to': idTo,
            'color': {
                'color': "#000000"
            },
            // 'length': 300,
            'id': ++this.edgeId
        }
        ++this.edgeId;
        if(label){
            edge['label'] = label;
        }
        if(arrow) {
            edge['arrows'] = "to"
        }


        this.edgeList.push(edge)
        return edge;
    }


    initEvents() {

        let usedSheet: number | null | undefined = null;

        let step;
        let nodePosition;
        this.network.on("afterDrawing", (ctx) => {
            for (step = 0; step < this.fixedNodes.length; step++) {
                nodePosition = this.network.getPositions([this.fixedNodes[step]]);
                ctx.strokeStyle = '#000000';
                ctx.lineWidth = 2;
                ctx.beginPath();
                try {
                    ctx.arc(
                        nodePosition[this.fixedNodes[step]].x,
                        nodePosition[this.fixedNodes[step]].y,
                        1,
                        0,
                        2 * Math.PI
                    );
                    ctx.stroke();
                } catch (e) {

                }

            }
        });
        this.network.on("selectNode", (params) => {
            // this.layoutHeight = '66'
            this.activeNode = this.getSelectedNode();
            console.log(this.activeNode)
            if(this.buffered){
                this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
                this.buffered = false;
            }

            this.switchMenuBar();
            this.updateMetadataLayout();
        });
        this.network.on('selectEdge', (params) => {
            const edgesId = this.network.getSelectedEdges();
            if(edgesId.length == 1 && this.network.getSelectedNodes().length == 0){
                this.activeEdge = this.edgeDataSet.get(edgesId[0]);
                // @ts-ignore
                let node = this.nodeDataSet.get(this.network.getConnectedNodes(this.activeEdge!.id!) as Id[]).filter(x => (x.type == NodeType.TIE || x.type == NodeType.TX_ANCHOR))
                if(node.length != 0){
                    this.edgeDialog = new EdgeEditorDialog();
                    this.edgeDialog.identifier= this.activeEdge!.label? this.activeEdge!.label.split(', ')[1].length == 3 : false;
                    this.edgeDialog.role=this.activeEdge!.label? this.activeEdge!.label.split(", ")[0] : null;
                    this.edgeDialog.dialogOpened = true;
                    this.activeNode = node[0];
                    let edgeRole;
                    if(this.activeEdge?.to!.toString().length == 3){
                        // @ts-ignore
                        edgeRole = this.activeNode.knotRole[0]
                        this.edgeDialog.description = edgeRole.description;
                    } else {
                        // @ts-ignore
                        edgeRole = this.activeNode.anchorRole.find((x: { type: IdType | undefined; }) => x.id == this.activeEdge?.id);
                        this.edgeDialog.description = edgeRole.description;
                    }
                    this.edgeDialog.identifier=edgeRole.identifier;
                    this.edgeDialog.role=edgeRole.role;
                }
            }
        });
        this.network.on("deselectNode", (params) => {
            this.activeNode = this.getSelectedNode();
            this.switchMenuBar();
            this.metadataLayoutVisibility = 'none';
            this.activeFieldName = null;
            this.renderingComponent = null;
            if (this.network.getSelectedNodes().length == 0) {
                this.activeNode = null;
                this.closeMetadataLayout();
                // this.layoutHeight = '97';
            }
        });
        this.network.on('doubleClick', (properties) => {
            const ids = properties.nodes;
            if (ids.length > 0) {
                const node = this.nodeDataSet.get(ids[0]);
                if (this.fixedNodes.includes(ids[0])) {
                    // @ts-ignore
                    node["fixed"] = false;
                    this.nodeDataSet.update(node);
                    // @ts-ignore
                    const index = this.fixedNodes.indexOf(ids[0]);
                    this.fixedNodes.splice(index, 1);
                } else {
                    this.fixedNodes.push(ids[0]);
                    // @ts-ignore
                    node["fixed"] = true;
                    const pos = this.network.getPositions([ids[0]]);
                    // @ts-ignore
                    node["x"] = pos[ids[0]].x;
                    // @ts-ignore
                    node["y"] = pos[ids[0]].y;
                    // @ts-ignore
                    this.nodeDataSet.update(node);
                }
            }
        });
        this.network.on('oncontext', (properties) => {

            const a = this.network.getNodeAt(properties.pointer.DOM);
            const b = this.network.getEdgeAt(properties.pointer.DOM);

        });
        this.network.on('zoom', (e) => {
            const pos = this.network.getViewPosition();
            if (this.network.getScale() > 1.5)//the limit you want to stop at
            {
                this.network.moveTo({
                    scale: 1.5,
                    position: {x:pos.x, y:pos.y}
                }); //set this limit so it stops zooming out here
            }
            if (this.network.getScale() < 0.2)//the limit you want to stop at
            {
                this.network.moveTo({
                    scale: 0.2,
                    position: {x:pos.x, y:pos.y}
                });  //set this limit so it stops zooming out here
            }
        });
        this.network.on('dragStart', (params) => {

            this.activeNode = this.getSelectedNode();
            // if(usedSheet != null){
            //     this.renderingComponent?.setSelectedSheet(usedSheet as number);
            // }
            usedSheet = this.renderingComponent?.getSelectedSheet();

            this.switchMenuBar();
            const ids = params.nodes;
            if (ids.length > 0) {
                // @ts-ignore
                const node = this.nodeDataSet.get(ids[0]);
                // @ts-ignore
                node["fixed"] = false;
                const pos = this.network.getPositions([ids[0]]);
                // @ts-ignore
                node["x"] = pos[ids[0]].x;
                // @ts-ignore
                node["y"] = pos[ids[0]].y;
                // @ts-ignore
                this.nodeDataSet.update(node);
            }
        });
        this.network.on('dragEnd', (properties) => {
            const ids = properties.nodes;
            this.activeNode = this.getSelectedNode();
            if(usedSheet != null){
                this.renderingComponent?.setSelectedSheet(usedSheet as number);
            }
            if (ids.length > 0) {
                if (this.fixedNodes.includes(ids[0])) {
                    // @ts-ignore
                    this.activeNode["fixed"] = true;
                    const pos = this.network.getPositions([ids[0]]);
                    // @ts-ignore
                        this.activeNode["x"] = pos[ids[0]].x;
                    // @ts-ignore
                    this.activeNode["y"] = pos[ids[0]].y;
                    this.nodeDataSet.update(this.activeNode);
                }
            }

        });
    }

    switchMenuBar() {
        if (this.network.getSelectedNodes().length == 1) {
            this.metadataLayoutVisibility = 'flex';
            // @ts-ignore
            this.chosenNodeType = this.activeNode.type;
        } else if (this.network.getSelectedNodes().length == 0) {
            this.chosenNodeType = NodeType.NO_TYPE;
        } else { // @ts-ignore
            if (this.network.getSelectedNodes().length > 1) {
                // @ts-ignore
                if(this.network.getSelectedNodes().length == 2){
                    if(this.isKnotThere()) {
                        this.chosenNodeType = NodeType.KNOT_AND_OTHERS;
                    } else if(this.isAnchorAndTie()){
                        this.chosenNodeType = NodeType.ANCHOR_AND_TIE; // @ts-ignore
                    } else if (this.allNodesIsTypedAs(this.activeNode.type)) { // @ts-ignore
                        if(this.activeNode.type == NodeType.ANCHOR){
                            this.chosenNodeType = NodeType.ANCHORS_GROUP; // @ts-ignore
                        } else if(this.activeNode.type == NodeType.TIE){
                            this.chosenNodeType = NodeType.TIE;
                        } else {
                            this.chosenNodeType = NodeType.KNOT;
                        }
                    } else {
                        this.chosenNodeType = NodeType.KNOT;
                    }
                    // @ts-ignore
                } else if (this.allNodesIsTypedAs(this.activeNode.type) && this.network.getSelectedNodes().length > 2 ) { // @ts-ignore
                    if(this.activeNode.type == NodeType.ANCHOR){
                        this.chosenNodeType = NodeType.ANCHORS_GROUP; // @ts-ignore
                    } else if(this.activeNode.type == NodeType.TIE){
                        this.chosenNodeType = NodeType.TIE;
                    } else {
                        this.chosenNodeType = NodeType.KNOT;
                    }
                } else {
                    this.chosenNodeType = NodeType.KNOT;
                }
            } else {
                this.chosenNodeType = NodeType.KNOT;
            }
            this.metadataLayoutVisibility = 'none';
            this.activeFieldName = null;
            // this.layoutHeight = '97';
        }
    }

    closeMetadataLayout() {
        this.activeNode = null;
        this.metadataLayoutVisibility = 'none';
        this.activeFieldName = null;
        if (this.buffered){
            this.history = updateHistory(this.edgeDataSet.get(), this.nodeDataSet.get());
            this.buffered = false;
        }
        // this.layoutHeight = '97';
    }
}

interface VisJsComponentServerInterface {
    fillComponentRequest(): void;
    updateKnot(s: string): void;
    addKnot(s: string): void;
    updateAnchor(s: string): void;
    addAnchor(s: string): void;
    updateTie(s: string): void;
    updateTxAnchor(s: string): void;
    addTie(s: string): void;
    addTxAnchor(s: string): void;

    deleteAnchor(s: string): void;
    deleteTie(s: string): void;
    deleteKnot(s: string): void;
    deleteTxAnchor(s: string): void;

    marshall(): void;
}

declare global {
    interface Window {
        showSaveFilePicker:any;
    }
}
