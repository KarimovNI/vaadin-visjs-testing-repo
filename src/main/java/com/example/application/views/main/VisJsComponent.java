package com.example.application.views.main;



import com.fasterxml.jackson.core.JsonProcessingException;

import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.upload.Upload;
import jakarta.xml.bind.JAXBException;
import org.leandi.datalandscape.DomainSchema;
import org.xml.sax.SAXException;


import java.io.*;

@JsModule("./vis-js-component.ts")
@NpmPackage(value = "vis-network", version = "9.1.2")
@Tag("custom-tag")
@Uses(Icon.class)
@Uses(MenuBar.class)
@Uses(Upload.class)
public class VisJsComponent extends Component {

    DomainSchema domainSchema = null;

    public VisJsComponent() throws IOException, SAXException { }

    @ClientCallable
    private void fillComponentRequest(){
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("dossier.xml").getFile());
//            InputStream inputStream = new FileInputStream("src/main/resources/dossier.xml");
            this.domainSchema = new DomainSchema(file);
            sendTree();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @ClientCallable
    private void marshall() {
        try {
            String xmlContent = this.domainSchema.marshall();
            getElement().executeJs("this.saveToFile($0)", xmlContent);
       } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
    @ClientCallable
    private void updateKnot(String knotJson) {
        this.domainSchema.updateKnot(knotJson);
    }
    @ClientCallable
    private void addKnot(String knotJson) {
        this.domainSchema.addKnot(knotJson);
    }
    @ClientCallable
    private void deleteKnot(String knotUid) {
        this.domainSchema.deleteKnot(knotUid);
    }

    @ClientCallable
    private void updateAnchor(String anchorJson) {
        this.domainSchema.updateAnchor(anchorJson);
    }
    @ClientCallable
    private void addAnchor(String anchorJson) {
        this.domainSchema.addAnchor(anchorJson);
    }
    @ClientCallable
    private void deleteAnchor(String anchorUid) {
        this.domainSchema.deleteAnchor(anchorUid);
    }

    @ClientCallable
    private void updateTie(String tieJson) {
        this.domainSchema.updateTie(tieJson);
    }
    @ClientCallable
    private void addTie(String tieJson) {
        this.domainSchema.addTie(tieJson);
    }
    @ClientCallable
    private void deleteTie(String tieUid) {
        this.domainSchema.deleteTie(tieUid);
    }

    @ClientCallable
    private void updateTxAnchor(String tieJson) {
        this.domainSchema.updateTxAnchor(tieJson);
    }
    @ClientCallable
    private void addTxAnchor(String txAnchorJson) {
        this.domainSchema.addTxAnchor(txAnchorJson);
    }
    @ClientCallable
    private void deleteTxAnchor(String txAnchorUid) {
        this.domainSchema.deleteTxAnchor(txAnchorUid);
    }


    public void sendTree() throws IOException {
        getElement().executeJs("this.getTree($0, $1, $2, $3)", domainSchema.getAnchors(),
            domainSchema.getKnots(), domainSchema.getTies(), domainSchema.getTxAnchors());
    }
}
